/*--------------------------------------------------------------------------*/
// canvas implementation
//declaring variables
var oCanvas = document.getElementById("thecanvas");
window.addEventListener('resize', resizeCanvas, false);
oCanvas.width = window.innerWidth;
oCanvas.height = window.innerHeight*(3/4);


var oCtx = oCanvas.getContext('2d');
var clickX = new Array();
var clickY = new Array();
var clickDrag = new Array();
var lineWidths = new Array();
var shadowBlurs = new Array();
var bMouseIsDown = false; //mouse is down
var canvasIsDirty = false;
var stroke_img = new Image();

var trailingBlur = 10;
var waterColorBlur = 20;
var topBarHeight = 60;	//This was causing the offset, the top bar was not accounted for in getting event Y coordinate
var aspectWidth = 16;
var aspectHeight = 9;


var curr_count = 0;

var canW = 0;
var canH = 0;

var oCanvas = document.getElementById("thecanvas");
var oCtx = oCanvas.getContext('2d');

resizeCanvas();

var lastPoint;//used for stroke_paintbrush
var timeInterval;//used for penguinFade()
var iWidth = oCanvas.width;
var iHeight = oCanvas.height;
var paintbrush_type = 0;  //type of paintbrush
var shadowBlur = 0;


//setting up initial parameters
oCtx.fillStyle = "rgb(255,255,255)";
oCtx.fillRect(0,0,iWidth,iHeight);

oCtx.strokeStyle = "rgb(0,0,0)";
oCtx.lineJoin = oCtx.lineCap = 'round';
oCtx.shadowBlur = shadowBlur;
oCtx.shadowColor = 'rgb(0, 0, 0)';

paintbrush_type = 0;
shadowBlur = 0;

oCanvas.addEventListener("mousedown", mouseDown);
oCanvas.addEventListener("mousemove", mouseMove);
oCanvas.addEventListener("mouseup", mouseUp);
oCanvas.addEventListener("mouseout", mouseOut);
//oCanvas.addEventListener("mouseover", mouseOver);
//oCanvas.addEventListener("mouseout", touchCancel);
oCanvas.addEventListener("touchstart", touchDown);
oCanvas.addEventListener("touchmove", touchMove);
oCanvas.addEventListener("touchend", touchUp);
oCanvas.addEventListener("touchcancel", touchCancel);


//selecting the shadow to create the watercolor paint effect
function selectShadow (id_of_paintbrush){
	if(paintbrush_type == 0)
		shadowBlur = 0;
	else if(paintbrush_type == 1)
		shadowBlur = 10;    
	else if(paintbrush_type == 2)
		shadowBlur = 0; 

	var elems = document.getElementsByTagName('*'), i;
	for (i in elems) {
		if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrush' + ' ')
				> -1) {
			if(elems[i].getAttribute('id') == id_of_paintbrush){
				elems[i].setAttribute("style", "opacity: 1; display: inline; ");
			}else{
				elems[i].setAttribute("style", "opacity: 0.6; display: inline; ");
			}
		}
	}
}

//selecting the size of the paintbrush
function selectSize(id_of_paintbrushSize){
	var elems = document.getElementsByTagName('*'), i;
	for (i in elems) {
		if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrushSize' + ' ')
				> -1) {
			if(elems[i].getAttribute('id') == id_of_paintbrushSize){
				elems[i].setAttribute("style", "opacity: 1; display: inline;");
			}else{
				elems[i].setAttribute("style", "opacity: 0.6; display: inline");
			}
		}
	}
}

//when the mouse is down
function mouseDown(e) {
	bMouseIsDown = true;
	canvasIsDirty = true;
	iLastX = e.offsetX || e.layerX;
	iLastY = e.offsetY || e.layerY;

	addClick(iLastX, iLastY);
	draw();
	//oCtx.moveTo(iLastX, iLastY);
	lastPoint = {x: iLastX, y: iLastY};
}

//When touch begins
function touchDown(e){
	e.preventDefault();
	bMouseIsDown = true;
	canvasIsDirty = true;
	
	iLastX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
	iLastY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
	addClick(iLastX, iLastY);
	draw();
	lastPoint = {x: iLastX, y: iLastY};
}

//when the mouse is up
function mouseUp(e) {
	bMouseIsDown = false;
	penguinFade();
}

//When the finger is lifted
function touchUp(e){
	bMouseIsDown = false;
	penguinFade();
}

//if the mouse is down and being dragged
function mouseMove(e) {
	if (bMouseIsDown && canvasIsDirty) {
		var iX = e.offsetX || e.layerX;
		var iY = e.offsetY || e.layerY;
		// oCtx.moveTo(iLastX, iLastY);
		oCtx.lineTo(iX, iY);
		oCtx.stroke();
		iLastX = iX;
		iLastY = iY;
	}
}

function touchMove(e){
	e.preventDefault();
	if (bMouseIsDown && canvasIsDirty) {
	var iX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
	var iY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;

		oCtx.lineTo(iX, iY);
		oCtx.stroke();
		iLastX = iX;
		iLastY = iY;
	}
}

function touchCancel(e) {
	bMouseIdDown = false;
	penguinFade();
}

function mouseOut(e) {
	bMouseIsDown = false;
	penguinFade();
}

//add these to each of the arrays
function addClick(x, y, dragging)
{
	clickX.push(x);
	clickY.push(y);
	clickDrag.push(dragging);
	lineWidths.push(lineWidth);
	shadowBlurs.push(shadowBlur);
}

//drawing new element in the canvas 
function draw(){
	var i = curr_count;
	oCtx.lineWidth = lineWidths[i]; 
	oCtx.shadowBlur = shadowBlurs[i];   
	oCtx.beginPath();
	if(clickDrag[i] && i){
		oCtx.moveTo(clickX[i-1], clickY[i-1]);
	}else{
		oCtx.moveTo(clickX[i]-1, clickY[i]);
	}
	oCtx.lineTo(clickX[i], clickY[i]);
	oCtx.closePath();
	oCtx.stroke();
	curr_count++;
}

//redrawing the canvas with the elements in the arrays
//we actually can't redraw because of our drawing implementation
//we cannot save all of our datapoints because we do not want
//to slow down the app. Thus, if you resize, you will lose your drawing
//instead of us keeping your drawing. However, this function is here in case
//a new future implementation of draw is done, where every point is saved.
function redraw(){
	oCtx.fillStyle = "rgb(255,255,255)";
	oCtx.fillRect(0,0,iWidth,iHeight);
	oCtx.strokeStyle = "rgb(0,0,0)";
	oCtx.lineJoin = oCtx.lineCap = 'round';
	oCtx.lineWidth = lineWidth;
	oCtx.shadowBlur = shadowBlur;
	oCtx.shadowColor = 'rgb(0, 0, 0)';
	for(var i=0; i < clickX.length; i++) {
		//var i = curr_count;
		oCtx.lineWidth = lineWidths[i]; 
		oCtx.shadowBlur = shadowBlurs[i];   
		oCtx.beginPath();
		if(clickDrag[i] && i){
			oCtx.moveTo(clickX[i-1], clickY[i-1]);
		}else{
			oCtx.moveTo(clickX[i]-1, clickY[i]);
		}
		oCtx.lineTo(clickX[i], clickY[i]);
		oCtx.closePath();
		oCtx.stroke();
		//curr_count++;
	}
}

//clearing the canvas
function clearCanvas(){
	oCtx.fillStyle = "rgb(255,255,255)";
	oCtx.fillRect(0,0,oCanvas.width,oCanvas.height);
	oCtx.strokeStyle = "rgb(0,0,0)";
	oCtx.lineJoin = oCtx.lineCap = 'round';
	oCtx.lineWidth = lineWidth;
	oCtx.shadowBlur = shadowBlur;
	oCtx.shadowColor = 'rgb(0, 0, 0)';
	clickX = [];
	clickY = [];
	clickDrag = [];
	lineWidths = [];
	shadowBlurs = [];
	curr_count = 0;
	canvasIsDirty = false;
}

function resizeStroke(x) {
	if(Math.min(oCtx.canvas.width, oCtx.canvas.height) >= 500)
		return x;
	return (Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * x);
	}

function resizeCanvas(){
	canW = window.innerWidth;
	canH = window.innerHeight - 60 - 58 - 55; //have to look into getting this magic numbers dynamically
																				//60: Menu bar height
																				//58: Tools icon height, 1nd row
																				//58: Tools icon height, 2st row, if screen too small
																					//Need to grab this from css somehow

	// console.log(window.innerHeight);
	// console.log(aspectHeight);
	// console.log(Math.floor(canH / aspectHeight));

	// console.log(window.innerWidth);
	// console.log(aspectWidth);
	// console.log(Math.floor(canW / aspectWidth));

	if(canH/aspectHeight <= canW/aspectWidth){
		oCtx.canvas.height = Math.floor(canH / aspectHeight) * aspectHeight;
		oCtx.canvas.width = Math.floor(canH / aspectHeight) * aspectWidth;
		console.log("h <= W")
		}
	else{
		oCtx.canvas.width = Math.floor(canW / aspectWidth) * aspectWidth;
		oCtx.canvas.height = Math.floor(canW / aspectWidth) * aspectHeight;
	}		

			document.getElementById("actualcanvasdiv").style.height = (oCtx.canvas.height).toString() + "px";
			document.getElementById("actualcanvasdiv").style.width = (oCtx.canvas.width).toString() + "px";
			document.getElementById("actualcanvasdiv").style.marginTop = (1).toString() + "%";
			document.getElementById("actualcanvasdiv").style.cssFloat="inherit";		

	console.log(oCtx.canvas.width);
	console.log(oCtx.canvas.height);

	selectShadow("normal_paintbrush");
	lineWidth = resizeStroke(30);
	oCtx.lineWidth = lineWidth;
	selectSize("size30");


	clearCanvas();
}

//creating the fade animation effect every 50 milliseconds
function fadeTimeout()  {
	var mydiv = document.getElementById('thecanvas');
	var curr_opacity = window.getComputedStyle(mydiv,null).getPropertyValue("opacity");
	//if(!canvasIsDirty)
	//	return;
	if (bMouseIsDown != 0)  {
		mydiv.style.opacity = 1;
		return;
	}
	else if(curr_opacity < 0.002){
		clearCanvas();
		mydiv.style.opacity = 1;
		return;
	}
	else{
		curr_opacity = curr_opacity - 0.001;
	}
	mydiv.style.opacity = curr_opacity;
	setTimeout(fadeTimeout, 50);
}

//calling fadeAnimation every 2 seconds
function penguinFade(){
	ptimeInterval = setTimeout(fadeAnimation, 2000);
}

//checking if mouse is down in order to call fadeTimeout
function fadeAnimation(){
	if ((bMouseIsDown == 0)  && (canvasIsDirty)) {
		setTimeout(fadeTimeout,50);
	}
}