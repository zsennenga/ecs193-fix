function saveCanvas(pCanvas, strType) {
	var bRes = false;
	if (strType == "PNG"){
		bRes = Canvas2Image.saveAsPNG(oCanvas);//, 1, 1000, 1000);
	}
	if (strType == "BMP")
		bRes = Canvas2Image.saveAsBMP(oCanvas);
	if (strType == "JPEG")
		bRes = Canvas2Image.saveAsJPEG(oCanvas);

	if (!bRes) {
		alert("Sorry, this browser is not capable of saving " + strType + " files!");
		return false;
	}
}

var Canvas2Image = (function() {

	// check if we have canvas support
	var bHasCanvas = false;
	var oCanvas = document.getElementById("thecanvas");
	if (oCanvas.getContext("2d")) {
		bHasCanvas = true;
	}

	// no canvas, bail out.
	if (!bHasCanvas) {
		return {
			saveAsBMP : function(){},
			saveAsPNG : function(){},
			saveAsJPEG : function(){}
		}
	}

	var bHasImageData = !!(oCanvas.getContext("2d").getImageData);
	var bHasDataURL = !!(oCanvas.toDataURL);
	var bHasBase64 = !!(window.btoa);

	var strDownloadMime = "image/octet-stream";

	var scaleCanvas = function(oCanvas, iWidth, iHeight) {
		if (iWidth && iHeight) {
			var oSaveCanvas = document.createElement("canvas");
			oSaveCanvas.width = iWidth;
			oSaveCanvas.height = iHeight;
			oSaveCanvas.style.width = iWidth+"px";
			oSaveCanvas.style.height = iHeight+"px";

			var oSaveCtx = oSaveCanvas.getContext("2d");

			oSaveCtx.drawImage(oCanvas, 0, 0, oCanvas.width, oCanvas.height, 0, 0, iWidth, iHeight);
			return oSaveCanvas;
		}
		return oCanvas;
	}

	return {

		saveAsPNG : function(oCanvas, bReturnImg, iWidth, iHeight) {
			if (!bHasDataURL) {
				console.log("bHasDataURL has no data");
				return false;
			}
			var oScaledCanvas = scaleCanvas(oCanvas, iWidth, iHeight);
			var strData = oScaledCanvas.toDataURL("image/png");
			if (bReturnImg) {
				//console.log("THERE WAS A BRETURNIMG AFTER ALL\n");
				//console.log(strData);
				return makeImageObject(strData);
			} else {
				//console.log("THERE WAS NO BRETURNIMG");
				var save_button = document.getElementById("savebut");
				save_button.href = strData;
				//  saveFile(strData.replace("image/png", strDownloadMime));
			}
			return true;
		}//,
	};

})();
