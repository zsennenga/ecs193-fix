
/* Responsive HTML5 gallery tutorial
 *
 * http://tomicloud.com/2014/01/responsive-gallery
 * https://github.com/tomimick/responsive-image-gallery */
var BASE_URL = "backend/API/";

$(function main() {
	var galCount = 0;
	var curr_count = 0;
	var local_search = 0;
	var imageGalleryInit = 0;
	var latitude = 0;
	var longitude = 0;
	var bool_location_exists = 0;
	var var_lat = '';
	var var_long = '';
	var penguinIDS = [];
	var plusSignExists = 0;
	var menu = $("#menu");
	var main = $("#main");
	var top = $("#top");
	var home = $("#home");
	var about = $("#about");
	var images = $("#images");
	var drawingCanvas = $("#drawingCanvas");
	var favorites = $("#favorites");
	var carousel = $("#carousel");
	var topsearch = $("#topsearch");
	var carousel_obj = new Carousel("#carousel");
	var is_favorites_active = false;

	// remove 300ms click delay in mobile browsers
	FastClick.attach(document.body);

	// associate all click handlers
	set_click_handlers();
	location.hash = "";

	// clone menu hierarchy for large screen
	$("#top").append(menu.find("ul").clone());

	// url hash has changed
	$(window).on('hashchange', function () {
		route_url();
	});

	// load images from flickr
	//fetch_images();

	// route to main page
	route_url();

	// resize handler
	$(window).on('resize orientationchange', function(){
		resize_images();
	});

 	navigator.geolocation.getCurrentPosition(success, error);
	
	if (!navigator.geolocation){
		console.log("<p>Geolocation is not supported by your browser</p>");
		return;
	}

	function success(position) {
		latitude  = position.coords.latitude;
		longitude = position.coords.longitude;
		var_lat = '' + latitude;
		var_long = '' + longitude;
	};

	function error() {
		console.log("Unable to retrieve your location");
	};

//	based on url hash, adjust layout
	function route_url() {
		var hash = window.location.hash;
		var fade_carousel = true;
		is_favorites_active = false;

		// select active menu item
		$("ul.menu").find("a").removeClass("active");
		var active = $("ul.menu").find("[href='"+(hash?hash:"#")+"']");
		active.addClass("active");
		top.find("h1 a").text(active.eq(0).text());

		if (!hash) {
			home.show();
			drawingCanvas.hide();
			$("#imageGallery").hide();
			favorites.hide();
			about.hide();
			top.hide();
			$("#topsearch, #main").removeClass("opensearch");
		} else if (hash == "#gallery"){
			clearCanvas();
			$(".ico").attr("href", "#gallery");
			$("#localbut").show();
			$("#randombut").show();
			$("#savebut").hide();
			$("#searchbut").show();
			$("#images").empty();
			penguinIDS = [];
			plusSignExists = 0;
			home.hide();
			drawingCanvas.hide();
			fetch_images();
			$("#imageGallery").show();
			about.hide();
			favorites.hide();
			top.show();
			resize_images();
		} else if (hash == "#drawingCanvas"){
			$(".ico").attr("href", "#drawingCanvas");
			$("#localbut").hide();
			$("#randombut").hide();
			$("#savebut").show();
			$("#searchbut").hide();
			$("#topsearch, #main").removeClass("opensearch");
			home.hide();
			drawingCanvas.show();
			$("#imageGallery").hide();
			about.hide();
			favorites.hide();
			top.show();
			penguinFade();
		} else if (hash == "#about") {
			clearCanvas();
			$(".ico").attr("href", "#about");
			$("#localbut").hide();
			$("#randombut").hide();
			$("#savebut").hide();
			$("#searchbut").hide();
			$("#topsearch, #main").removeClass("opensearch");
			home.hide();
			drawingCanvas.hide();
			$("#imageGallery").hide();
			about.show();
			favorites.hide();
			top.show();
		} else if (hash == "#favorites") {
			is_favorites_active = true;
			$("#topsearch, #main").removeClass("opensearch");
			$("#imageGallery").hide();
			about.hide();
			favorites.show();
			fill_favorites();
			resize_images();
		} else if (hash.slice(0,5) == "#view") {
			// view image at index
			var i = parseInt(hash.slice(6));
			var div = $("#images").find(">div").eq(i);
			show_carousel(div);
			about.hide();
			favorites.hide();
			fade_carousel = false;
		}

		if (fade_carousel && carousel.hasClass("anim2"))
			hide_carousel();
	}

	
	//Sets paintbrush strokes relative to how they would look unchanged on a square canvas of width 500
	//If the smaller dimension is greater than 500, don't upscale the paintbrush size

function resizeStroke(x) {
	if(Math.min(oCtx.canvas.width, oCtx.canvas.height) >= 500)
		return x;
	return (Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * x);
	}



//	sets all click handlers
	function set_click_handlers() {
		$("#show_gallery").click(function(){
			clearCanvas(); //clearing the canvas
			window.location.hash = "#gallery";
			// $("ul.menu").find("a").removeClass("active");
			// var active = $("ul.menu").find("[href='#gallery']");
			// active.addClass("active");
			// top.find("h1 a").text(active.eq(0).text());
			// $(".ico").attr("href", "#gallery");
			// $("#localbut").show();
			// $("#randombut").show();
			// $("#savebut").hide();
			// penguinIDS = [];
			// plusSignExists = 0;
			// home.hide();
			// fetch_images();
			// images.show();
			// drawingCanvas.hide();
			// top.show();
			// resize_images();
		});

		$("#show_canvas").click(function(){
			clearCanvas(); //clearing the canvas
			window.location.hash = "#drawingCanvas";
			// $("ul.menu").find("a").removeClass("active");
			// var active = $("ul.menu").find("[href='#drawingCanvas']");
			// active.addClass("active");
			// top.find("h1 a").text(active.eq(0).text());
			// $(".ico").attr("href", "#drawingCanvas");
			// $("#localbut").hide();
			// $("#randombut").hide();
			// $("#savebut").show();
			// home.hide();
			// images.hide();
			// drawingCanvas.show();
			// top.show();
			// penguinFade();
		});

		$("#show_about").click(function(){
			clearCanvas(); //clearing the canvas
			window.location.hash = "#about";
			$("ul.menu").find("a").removeClass("active");
			var active = $("ul.menu").find("[href='#about']");
			active.addClass("active");
			top.find("h1 a").text(active.eq(0).text());
			$(".ico").attr("href", "#about");
			$("#localbut").hide();
			$("#randombut").hide();
			$("#savebut").hide();
			home.hide();
			images.hide();
			drawingCanvas.hide();
			about.show();
			top.show();
		});
		$("#size10").click(function(){
			lineWidth = resizeStroke(10); selectSize("size10");
		});
		$("#size20").click(function(){
			lineWidth = resizeStroke(20); selectSize("size20");
		});
		$("#size30").click(function(){
			lineWidth = resizeStroke(30); selectSize("size30");
		});
		$("#size40").click(function(){
			lineWidth = resizeStroke(40); selectSize("size40");
		});
		$("#size50").click(function(){
			lineWidth = resizeStroke(50); selectSize("size50");
		});
		$("#normal_paintbrush").click(function(){
			paintbrush_type = 0; selectShadow("normal_paintbrush");
		});
		$("#wc_paintbrush").click(function(){
			paintbrush_type = 1; selectShadow("wc_paintbrush");
		});

		// small screen: open/hide sliding menu
		$("#menubut, #menu a").click(function(){
			$("#main,#menu,#top,#topsearch").toggleClass("openmenu").removeClass("opensearch");
			if ($(this).attr("id") == "menubut")
				return false;
		});

		// reload all
		$("#reloadbut").click(function(){
			reload();
		});

		$("#localbut").click(function(){
			setTimeout(function(){
				$("#topsearch input").focus();
			}, 500);
			$("#topsearch").removeClass("opensearch");
			main.removeClass("opensearch");
			$("#images").empty();
			penguinIDS = [];
			plusSignExists = 0;
			local_search = 1;
			locateLocalPenguins();
		});

		$("#randombut").click(function(){
			setTimeout(function(){
				$("#topsearch input").focus();
			}, 500);
			$("#topsearch").removeClass("opensearch");
			main.removeClass("opensearch");
			$("#images").empty();
			penguinIDS = [];
			plusSignExists = 0;
			local_search = 0;
			random();
		});

		$("#savebut").click(function(){
			if(canvasIsDirty == true){
				save();
			}else{ alert("ERROR, YOU CANNOT SAVE AN EMPTY CANVAS")}
			
		});

		// open up search
		$("#searchbut").click(function(){

			$("#topsearch input").val("");
			if ($("#topsearch").toggleClass("opensearch").hasClass("opensearch"))
				setTimeout(function(){
					$("#topsearch input").focus();
				}, 500);
			main.toggleClass("opensearch");
		});

		// click on image
		$("#images, #favorites").on("click", "div", function(t) {
			var div = $(t.target);

			if (!div.is("div.my"))
				div = div.parents("div");
			if (!div.hasClass("my"))
				return;

			build_carousel(is_favorites_active ? favorites : images);

			if (div.hasClass("more")){
				plusSignExists = 0;
				load_more(div);
			}
			else
				window.location.hash = "#view?" + div.index();
		});

		// click on image area closes menu
		main.on("click", function() {
			if (main.hasClass("openmenu")) {
				$("#menubut").click();
				return false;
			}
		});

		// enter in search activates search
		$("#searchinput").keyup(function(e) {
			if (e.keyCode == 13) {
				//alert("pressed search and got: " + document.getElementById("searchinput").value);
				images.empty();
				penguinsIDS = [];
				$.getJSON( BASE_URL + "penguin" + "?count=10&name=" + document.getElementById("searchinput").value, function( data ) {
					if(data.code == 0) {
						//$("<i>").attr("class", "search_type").text("Penguins With The Name " + document.getElementById("searchinput").value).appendTo($("#images"));						
							$.each( data.message, function( i, image ) {
								if(penguinMatchID(data.message[i].id) == 0){
									$("<div>").attr("class", "my show").attr("id", "pengallery"+galCount).appendTo($("#images"));
									$( "<img>" ).attr("class", "galleryImage").attr( "src", "backend/images/" + data.message[i].image ).appendTo("#pengallery"+galCount);
									$("<div>").text("penguin name: " + data.message[i].name).appendTo("#pengallery"+galCount);
									galCount++;
									penguinIDS[data.message[i].id] = data.message[i].id;
								}
							});
							
							if(plusSignExists == 0){
								images.append($("#morediv").clone().removeAttr("id"));
								penguinIDS["morediv"] = "morediv";
								plusSignExists = 1;
							}
							resize_images();

							
					} else{ alert("Sorry, there was an internal error. Please try again later.");}
				})
				.error(function() { console.log("Handle this!!"); })
				//function to actually SEARCH for penguins
			}
		});

		// escape always goes to home
		$(document.body).keyup(function(e) {
			if (e.keyCode == 27) {
				window.location.hash = "#gallery";
			}
		});

	}

//	toggle favorite-status while in carousel
	function toggle_favorite(t) {
		// t.toggleClass("favorited");
		// var url = t.closest("li").find("img").attr("src");
		// var title = t.closest("li").find("div").html();

		// if (t.hasClass("favorited"))
		//     add_favorite(url, title);
		// else
		//     remove_favorite(url);
	}

	function toggle_flag(t){
		t.toggleClass("flagged");

		if(t.hasClass("flagged")){
			//post flag information to backend
			var penguin_id = t.find("img").attr("id");
			 $.post(BASE_URL + "/flag/" + penguin_id, {id: penguin_id}, function(data) {
			 	if(data.code != 0){
			 		console.log("Can't flag this penguin");
			 	}
			 });
		}
	}

//	show carousel, position is at div
	function show_carousel(div) {
		carousel_obj.init();
		carousel.hide().removeClass("anim1 anim2");

		// initial image index
		var i = div.index();
		carousel_obj.showPane(i, false);

		// show zoom anim
		carousel.addClass("anim1");
		carousel.show();
		carousel.width(); // flush
		carousel.addClass("anim2");

		$(window).scrollTop(0);
	}

	function hide_carousel() {
		carousel.removeClass("anim2");

		carousel.one("webkitTransitionEnd mozTransitionEnd transitionend", function(){
			// hide at end of transition
			carousel.hide();
		});
	}

//	resize images (width changed by CSS, height by this js)
	function resize_images() {
		var cont = is_favorites_active ? favorites : images;
		var w = cont.find("div.my").eq(0).width();
		cont.find("div.my").height(w);
	}

//	discard and reload all images
	function reload() {
		images.find("div").addClass("fadeaway");
		plusSignExists = 0;
		if(local_search){
			setTimeout(function() {
				images.empty();
				penguinIDS = [];
				locateLocalPenguins();
			}, 1000);
		}else{
			setTimeout(function() {
				images.empty();
				penguinIDS = [];
				fetch_images();
			}, 1000);
		}
	}

	function locateLocalPenguins(){
		//search for local penguins
		$.getJSON( BASE_URL + "penguin" + "?count=10&lat=" + var_lat + "&long=" + var_long, function( data ) {
			if(data.code == 0) {
				//$("<i>").attr("class", "search_type").text("Penguins Near You").appendTo($("#images"));
				$.each( data.message, function( i, image ) {
					if(penguinMatchID(data.message[i].id) == 0){
						$("<div>").attr("class", "my show").attr("id", "pengallery"+galCount).appendTo($("#images"));
						$( "<img>" ).attr("class", "galleryImage").attr( "src", "backend/images/" + data.message[i].image ).appendTo("#pengallery"+galCount);
						$("<div>").text("penguin name: " + data.message[i].name).appendTo("#pengallery"+galCount);
						galCount++;
						penguinIDS[data.message[i].id] = data.message[i].id;
					}
				});
			
				if(plusSignExists == 0){
					images.append($("#morediv").clone().removeAttr("id"));
					penguinIDS["morediv"] = "morediv";
					plusSignExists = 1;
				}
				resize_images();
			} else{ alert("Sorry, there was an internal error. Please try again later.");}
		})
		.error(function() { console.log("Handle this!!"); })
		// $.getJSON( galleryURL, function( data ) {
		// 	if(data.code == 0){
		// 		$.each( data.message, function( i, image ) {
		// 			$("<div>").attr("class", "my show").attr("id", "pengallery"+galCount).appendTo($("#images"));
		// 			$( "<img>" ).attr( "src", data.message[i].image ).appendTo("#pengallery"+galCount);
		// 			$("<div>").text("penguin "+galCount).appendTo("#pengallery"+galCount);
		// 			galCount++;
		// 		});
		// 		// });
				
		// 		resize_images();
		// 		images.append($("#morediv").clone().removeAttr("id"))
		// 	} else{ alert("Sorry, there was an internal error. Please try again later.");}
			
		// });
	}

	function random(){
		//fetch penguins randomly
		fetch_images();
	}

	function save(){
		//post penguin to backend
		var result = prompt("Name your penguin");
		$.post(BASE_URL + "penguin", { lat: latitude, long: longitude, penguinImage: oCanvas.toDataURL("image/jpeg"), name: result }, function(){ clearCanvas(); })
		.error(function() { console.log("Not creating penguin correctly. Handle this!!"); });
	}

//	load more images
	function load_more(div) {
		if(div.find("i").hasClass("cg")){

		}else{
			div.find("i").addClass("fa-refresh");
		}
	
		div.addClass("start");

		// can't use this since other can fire it
//		div.one("webkitTransitionEnd mozTransitionEnd transitionend", function(){
		if(local_search == 1){
			setTimeout(function() {
				images.find(".more").remove();
				locateLocalPenguins();
			}, 700);
		}
		else{
			setTimeout(function() {
				images.find(".more").remove();
				fetch_images();
			}, 700);
		}

	}

//  find matching penguinID
	function penguinMatchID(penguin_id){	
		if(penguinIDS[penguin_id] == penguin_id){
			return 1;
		}
		return 0;
	}
	
//	fetch images from Flickr
	function fetch_images() {
		$.getJSON( BASE_URL + "penguin" + "?count=10", function( data ) {
			if(data.code == 0) {
				//$("<i>").attr("class", "search_type fa-7x").attr("id", "fetch_images_text").text("Random Penguins").appendTo($("#imageGallery"));
				$.each( data.message, function( i, image ) {
					if(penguinMatchID(data.message[i].id) == 0){
						$("<div>").attr("class", "my show").attr("id", "pengallery"+galCount).appendTo($("#images"));
						$( "<img>" ).attr("class", "galleryImage").attr("id", data.message[i].id).attr( "src", "backend/images/" + data.message[i].image ).appendTo("#pengallery"+galCount);
						$("<div>").text("penguin name: " + data.message[i].name).appendTo("#pengallery"+galCount);
						galCount++;
						penguinIDS[data.message[i].id] = data.message[i].id;
					}
				});
				
				if(plusSignExists == 0){
					images.append($("#morediv").clone().removeAttr("id"));
					penguinIDS["morediv"] = "morediv";
					plusSignExists = 1;
				}
				resize_images();
			} else{ alert("Sorry, there was an internal error. Please try again later.");}
		})
		.error(function() { console.log("Handle this!!"); })
	}

//	build carousel <ul> from images in home/favorites
	function build_carousel(container) {

		var ul = carousel.find("ul");
		ul.empty();

		container.find("div.my img").each(function(i, elem) {
			var t = $(elem).clone();
			ul.append(t);
			ul.find("img:last").wrap("<li/>");
			var li = ul.find("li:last");
			li.append("<div>"+$(elem).parent().find("div").html()+"</div>");

			// favorited?
			var f = "";
			var url = t.attr("src");
			if (is_favorite(url))
				f = "favorited";

			/* XXX fix portrait pics...
        if (t.height() > t.width())
             t.addClass("portrait");
			 */

			li.append("<i class='fa fa-3x fa-times "+f+"'></i>");
			li.append("<i class='fa fa-3x fa-flag'></i>");
			// li.append("<i class='fa fa-3x fa-star "+f+"'></i>");
		});
	}


//	fill favorites div with data in localstorage
	function fill_favorites() {
		var data = getdata();

		favorites.empty();

		if (!data.length) {
			favorites.append($("#favoempty").clone());
			return;
		}

		for (var i = 0; i < data.length; i++) {
			var obj = data[i];

			var elem = $("<div class='my show favorite'><img src='"+obj.url+"'/><div>"+
					obj.title+"</div>");
			favorites.append(elem);
		}
	}

	function add_favorite(url, title) {
		var data = getdata();
		data.push({"title":title, "url":url});
		localStorage.setItem("imgdata", JSON.stringify(data));

		fill_favorites();
	}

	function remove_favorite(url) {
		var data = getdata();
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			if (url == obj.url) {
				data.splice(i, 1);
				break;
			}
		}

		localStorage.setItem("imgdata", JSON.stringify(data));

		fill_favorites();
	}

	function is_favorite(url) {
		var data = getdata();
		for (var i = 0; i < data.length; i++) {
			var obj = data[i];
			if (url == obj.url)
				return true;
		}
	}
//	return array containing favorite objects
	function getdata() {
		var data = localStorage.getItem("imgdata");
		if (!data)
			return [];
		return JSON.parse(data);
	}


	/*--------------------------------------------------------------------------*/
//	carousel component from hammer.js demo


//	handle taps of <i> in the carousel
	function handle_tap(el) {
		if (!el.is("i"))
			// window.history.back();
			window.location.hash = "#gallery";
		else if (el.is(".fa-times"))
			window.location.hash = "#gallery";
		// window.history.back();
		else if(el.is(".fa-flag"))
			toggle_flag(el);
		//else if (el.is(".fa-star"))
		//toggle_favorite(el);
		else if (el.is(".fa-arrow-circle-o-left"))
			carousel_obj.prev();
		else if (el.is(".fa-arrow-circle-o-right"))
			carousel_obj.next();
	}

	/**
	 * super simple carousel
	 * animation between panes happens with css transitions
	 */
	function Carousel(element)
	{
		var self = this;
		element = $(element);

		var container = $(">ul", element);
		var panes = $(">ul>li", element);

		var pane_width = 0;
		var pane_count = panes.length;

		var current_pane = 0;


		/**
		 * initial
		 */
		this.init = function() {
			panes = $(">ul>li", element);
			pane_count = panes.length;

			setPaneDimensions();

			$(window).on("load resize orientationchange", function() {
				setPaneDimensions();
				//updateOffset();
			});
		};


		/**
		 * set the pane dimensions and scale the container
		 */
		function setPaneDimensions() {
			pane_width = element.width();
			panes.each(function() {
				$(this).width(pane_width);
			});
			container.width(pane_width*pane_count);
		}

		/**
		 * show pane by index
		 */
		this.showPane = function(index, animate) {
			// between the bounds
			index = Math.max(0, Math.min(index, pane_count-1));
			current_pane = index;

			var offset = -((100/pane_count)*current_pane);
			setContainerOffset(offset, animate);
		};


		function setContainerOffset(percent, animate) {
			container.removeClass("animate");

			if(animate) {
				container.addClass("animate");
			}

			if(Modernizr.csstransforms3d) {
				container.css("transform", "translate3d("+ percent +"%,0,0) scale3d(1,1,1)");
			}
			else if(Modernizr.csstransforms) {
				container.css("transform", "translate("+ percent +"%,0)");
			}
			else {
				var px = ((pane_width*pane_count) / 100) * percent;
				container.css("left", px+"px");
			}
		}

		this.next = function() { return this.showPane(current_pane+1, true); };
		this.prev = function() { return this.showPane(current_pane-1, true); };

		function handleHammer(ev) {
			// disable browser scrolling
			if (ev.gesture)
				ev.gesture.preventDefault();

			switch(ev.type) {
			case 'dragright':
			case 'dragleft':
				// stick to the finger
				var pane_offset = -(100/pane_count)*current_pane;
				var drag_offset = ((100/pane_width)*ev.gesture.deltaX) / pane_count;

				// slow down at the first and last pane
				if((current_pane == 0 && ev.gesture.direction == "right") ||
						(current_pane == pane_count-1 && ev.gesture.direction == "left")) {
					drag_offset *= .4;
				}

				setContainerOffset(drag_offset + pane_offset);
				break;

			case 'swipeleft':
				self.next();
				ev.gesture.stopDetect();
				break;

			case 'swiperight':
				self.prev();
				ev.gesture.stopDetect();
				break;

			case 'tap':
				handle_tap($(ev.target));
				break;

			case 'release':
				// more then 50% moved, navigate
				if(Math.abs(ev.gesture.deltaX) > pane_width/2) {
					if(ev.gesture.direction == 'right') {
						self.prev();
					} else {
						self.next();
					}
				}
				else {
					self.showPane(current_pane, true);
				}
				break;
			default:
				break;
			}
		}

		var hammertime = new Hammer(element[0], { drag_lock_to_axis: true,
			drag_block_vertical: true});
		hammertime.on("release dragleft dragright swipeleft swiperight tap",
				handleHammer);
	}

}).error(function(){console.log("ERROR! MAIN NOT LOADED")});


