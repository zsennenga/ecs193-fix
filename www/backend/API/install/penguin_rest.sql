SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
CREATE DATABASE IF NOT EXISTS `penguin_rest` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `penguin_rest`;

DROP TABLE IF EXISTS `penguins`;
CREATE TABLE IF NOT EXISTS `penguins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(260) NOT NULL,
  `name` varchar(128) NOT NULL,
  `views` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `flagCount` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

DROP TABLE IF EXISTS `penguin_geo`;
CREATE TABLE IF NOT EXISTS `penguin_geo` (
  `id` int(11) NOT NULL,
  `pos` point NOT NULL,
  PRIMARY KEY (`id`),
  SPATIAL KEY `pos` (`pos`),
  SPATIAL KEY `pos_2` (`pos`),
  SPATIAL KEY `pos_3` (`pos`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `penguin_ips`;
CREATE TABLE IF NOT EXISTS `penguin_ips` (
  `ip` varchar(12) NOT NULL,
  `flagList` varchar(4096) NOT NULL,
  `time` varchar(12) NOT NULL,
  PRIMARY KEY (`ip`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `penguin_log`;
CREATE TABLE IF NOT EXISTS `penguin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `ip` varchar(64) NOT NULL,
  `method` varchar(10) NOT NULL,
  `route` varchar(260) NOT NULL,
  `params` varchar(4096) NOT NULL,
  `response` varchar(4096) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;
