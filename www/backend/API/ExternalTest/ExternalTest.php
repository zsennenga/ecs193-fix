<?php
require '../vendor/autoload.php';
require_once '../../Config.php';

use PenguinAPI\Models\DB\DBWrapper;

$db = new DBWrapper();
$c = curl_init();
curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
assert_options(ASSERT_ACTIVE, 1);
assert_options(ASSERT_WARNING, 0);
assert_options(ASSERT_CALLBACK, 'handle');

function handle($file, $line, $code, $desc = null)
{
    echo "Assertion failed at $file:$line: $code";
    if ($desc) {
        echo ": $desc";
    }
    echo "\n";
}

function rrmdir($dir) {
        if (is_dir($dir)) {
                $objects = scandir($dir);
                foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                                if (filetype($dir."/".$object) == "dir") rrmdir($dir."/".$object);
                                else unlink($dir."/".$object);
                        }
                }
                reset($objects);
                rmdir($dir);
        }
}

function idIn($out, $id)	{
	$out = $out['message'];
	foreach ($out as $p)	{
		if ($p['id'] == $id)
			return true;
	}
	return false;
}
$url = "http://localhost" . ROUTE_PREFIX . "/";

//Create penguin
//Test no uploaded file
echo "Testing no image\n";
curl_setopt($c, CURLOPT_POST, true);
$f = array();
$f['lat'] = 1;
$f['long'] = 1;
$f['name'] = 'steve';
curl_setopt($c, CURLOPT_POSTFIELDS, $f);
curl_setopt($c, CURLOPT_URL, $url . "penguin");
$out = curl_exec($c);
$out = json_decode($out, true);
assert($out['message'] === "No image uploaded", "No image uploaded");

//Test uploaded file
echo "Testing create penguin\n";
$f = array();
$f['lat'] = 1;
$f['long'] = 1;
$f['name'] = 'steve';
$p = file_get_contents('testPenguin.jpg');
$f['penguinImage'] = 'data:image/jpeg;base64,' . base64_encode($p);
curl_setopt($c, CURLOPT_POSTFIELDS, $f);
curl_setopt($c, CURLOPT_URL, $url . "penguin");
$out = curl_exec($c);
$out = json_decode($out, true);
assert(is_numeric($out['message']),  "Id not numeric");
$id = $out['message'];


//Test that I can't flood create penguins
$f = array();
$f['lat'] = 1;
$f['long'] = 1;
$f['name'] = 'steve';
$p = file_get_contents('testPenguin.jpg');
$f['penguinImage'] = 'data:image/jpeg;base64,' . base64_encode($p);
curl_setopt($c, CURLOPT_POSTFIELDS, $f);
curl_setopt($c, CURLOPT_URL, $url . "penguin");
$out = curl_exec($c);
$out = json_decode($out, true);
assert($out['message'] === 'You are creating Penguins too fast', 'You are creating Penguins too fast');

//Test Flagging it
echo "Test flagging Penguin\n";
curl_setopt($c, CURLOPT_URL, $url . "flag/".$id);
$out = curl_exec($c);
$out = json_decode($out, true);
assert($out['message'] === "",  "Flag response was not blank");

//Test that I can't flag it twice
echo "Test flagging Penguin Twice\n";
curl_setopt($c, CURLOPT_URL, $url . "flag/".$id);
$out = curl_exec($c);
$out = json_decode($out, true);
assert($out['message'] === 'IP has already flagged this image', 'IP has already flagged this image');

//Get it by ID
echo "Testing get by id\n";
curl_setopt($c, CURLOPT_POST, false);
curl_setopt($c, CURLOPT_HTTPGET, true);
curl_setopt($c, CURLOPT_URL, $url . "penguin/".$id);
$out = curl_exec($c);
$out = json_decode($out, true);
assert($out['message']['id'] == $id,  "Incorrect ID returned");

//Test nearby
echo "Test nearby Penguins\n";
curl_setopt($c, CURLOPT_HTTPGET, true);
curl_setopt($c, CURLOPT_URL, $url . "penguin/?lat=1&long=1&count=1");
$out = curl_exec($c);
$out = json_decode($out, true);
assert(idIn($out,$id), "Incorrect ID returned");


//Check that all those actions were logged
//Cleanup
$db->addParam(":ip", '127.0.0.1');
$db->query("DELETE FROM " . DB_IP_LOG . " WHERE ip = :ip");
$db->addParam(":id", $id);
$db->query("DELETE FROM " . DB_TABLE_PENGUIN . " WHERE id = :id");
$db->addParam(":id", $id);
$db->query("DELETE FROM " . DB_TABLE_GEO . " WHERE id = :id");
$db->query("TRUNCATE " . DB_TABLE_LOG);
//remove penguin images
rrmdir('../images');
