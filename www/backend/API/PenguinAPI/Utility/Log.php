<?php
namespace PenguinAPI\Utility;
/**
 * 
 * Logs certain parameters in the database after each request
 * 
 * @author its-zach
 * 
 */
class Log	{
	private $dbHandle;
	
	private $route;
	private $response;
	private $params;
	private $method;
	private $ip;
	
	public function __construct($dbHandle, $ip)	{
		$this->dbHandle = $dbHandle;
		$this->ip = $ip;
		$this->route = "";
		$this->response = "";
		$this->params = "";
		$this->method = "";
	}
	
	public function setRoute($r,$m)	{
		$this->route = $r;
		$this->method = $m;
	}
	
	public function setResp($res)	{
		if (is_array($res)) $this->response = serialize($res);
		else $this->response = $res;
	}
	
	public function setParams($p)	{
		$p = http_build_query($p);
		$this->params = $p;
	}
	/**
	 * Logs the request using the variables set with all the setters
	 */
	public function execute()	{
		$this->dbHandle->addParam(":ip", $this->ip);
		$this->dbHandle->addParam(":route", $this->route);
		$this->dbHandle->addParam(":method", $this->method);
		$this->dbHandle->addParam(":params", $this->params);
		$this->dbHandle->addParam(":response", $this->response);
		try	{
			$this->dbHandle->query("INSERT INTO ". DB_TABLE_LOG ." (ip,route,method,params,response) VALUES (:ip,:route,:method,:params,:response)");
		}
		catch (PenguinAPI\Exceptions\BaseException $e)	{
		}
	}
}
?>