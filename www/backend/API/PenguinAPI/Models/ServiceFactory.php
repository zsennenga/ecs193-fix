<?php
namespace PenguinAPI\Models;

use PenguinAPI\Exceptions\ParameterException;
use PenguinAPI\Exceptions\BaseException;
use PenguinAPI\Exceptions\FloodException;

/**
 *
 * ServiceFactory
 *
 * Class to connect the DataMapperFactory and the DomainObjectFactory
 *
 * Manages the passing of Http Documents to their respective parsers and for
 * telling the Data mappers to go GET and POST things.
 *
 * @author its-zach
 *
 */
class ServiceFactory {
	
	private $dataMapperFactory;
	private $domainObjectFactory;
	
	/**
	 * Builds the service factory
	 *
	 *
	 * @param DomainObjectFactory $do
	 * @param DataMapperFactory $dm
	 */
	public function __construct(DomainObjectFactory $do, DataMapperFactory $dm)	{
		$this->dataMapperFactory = $dm;
		$this->domainObjectFactory = $do;
	}
	/**
	 * Takes some image data and saves it to the images folder
	 * 
	 * @param $uploadedImage temp Image upload path
	 * @returns imagePath
	 */
	public function saveImage($uploadedImage, $path)	{
		$imageBuilder = $this->dataMapperFactory->get('ImageBuilder');
		$imageValidator = $this->domainObjectFactory->get('ImageValidator');
		$imageBuilder->setImage($uploadedImage);
		$imageBuilder->setPath($path);
		if (!$imageValidator->validateImage())	{
			throw new ParameterException(ERROR_PARAM, "Image provided to the API is not a valid Penguin.", "400");
		}
		return $imageBuilder->processImage();
	}
	/**
	 * Checks that an ip hasn't flooded the server, then creates a penguin if possible
	 * 
	 * @param Image URL $image
	 * @throws \PenguinAPI\Exceptions\BaseException
	 * @return ID of penguin
	 */
	public function makePenguin($image, $lat, $long, $ip, $name)	{
		$penguinTable = $this->dataMapperFactory->get('PenguinTable');
		$ipTable = $this->dataMapperFactory->get('IPTable');
		
		$this->checkLongLat($lat, $long);
		
		if ($ipTable->ipCanCreate($ip,true))	{
			$id = $penguinTable->addPenguin($image, $lat, $long, $name);
		}
		else	{
			throw new FloodException(ERROR_IP, "You are creating Penguins too fast", "401");
		}
		return $id;
	}
	/**
	 * Returns a list of penguin ids.
	 * 
	 * @param bool $method, true for random, false for location
	 * @param int $count,number to return
	 */
	public function getPenguinIds($count = 10, $lat = null, $long = null)	{
		$penguinTable = $this->dataMapperFactory->get('PenguinTable');
		$count = intval($count);
		if ($count <= 0 || $count >= 100)	{
			$count = 10;
		}
		if ($lat == null || $long == null)	{
			$out = $penguinTable->getRandomPenguins($count);
		}
		else	{
			$this->checkLongLat($lat, $long);
			$out = $penguinTable->getLocalPenguins($count, $lat, $long);
		}
		return $out;
	}
	/**
	 * Gets a penguin's data via it's id
	 * 
	 * @param PenguinID $id
	 * @throws \PenguinAPI\Exceptions\BaseException
	 * @return array
	 */
	public function getPenguinById($id)	{
		$penguinTable = $this->dataMapperFactory->get('PenguinTable');
		if (!is_numeric($id))	{
			throw new ParameterException(ERROR_PARAM, "ID must be numeric", "400");
		}
		$data = $penguinTable->getPenguin($id);
		return $data;
	}
	
	/**
	 * Flag an image if the source ip hasn't already flagged it.
	 * 
	 * @param PenguinID $id
	 * @throws \PenguinAPI\Exceptions\BaseException
	 * @return string
	 */
	public function flagImage($id, $ip)	{
		$ipTable = $this->dataMapperFactory->get('IPTable');
		$penguinTable = $this->dataMapperFactory->get('PenguinTable');
		if (!is_numeric($id))	{
			throw new ParameterException(ERROR_PARAM, "ID must be numeric", "400");
		}
		if (!$ipTable->ipCanFlag($id, $ip, true))	{
			throw new FloodException(ERROR_IP, "IP has already flagged this image", "400");
		}
		
		$penguinTable->flagPenguin($id);
		return "";
	}
	/**
	 * Checks that longitude/latitude are both set and numeric
	 * @throws \PenguinAPI\Exceptions\BaseException
	 */
	private function checkLongLat($lat, $long)	{
		if (!isset($long) || !isset($lat))	{
			throw new ParameterException(ERROR_PARAM, "Longitude and Latitude required", "400");
		}
		
		if (!is_numeric($long) || !is_numeric($lat))	{
			throw new ParameterException(ERROR_PARAM, "Longitude and Latitude must be numeric", "400");
		}
	}
}
?>