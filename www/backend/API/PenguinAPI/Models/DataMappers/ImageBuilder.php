<?php
namespace PenguinAPI\Models\DataMappers;

use PenguinAPI\Exceptions\ParameterException;
use PenguinAPI\Exceptions\BaseException;
class ImageBuilder	{
	
	private $fileData;
	private $fileName;
	private $path;
	
	public function __construct()	{
	}
	/**
	 * Sets the uploaded image the class will use
	 *  
	 * @param unknown $path
	 * @throws ParameterException
	 */
	public function setImage($b64)	{
		try	{
			$b64 = str_replace('data:image/jpeg;base64,', '', $b64);
			$b64 = base64_decode($b64);
			$this->fileData = imagecreatefromstring($b64);
		} catch (\Exception $e)	{
			throw new ParameterException(ERROR_PARAM, "Uploaded image is invalid", "500");
		}
		$this->fileName = md5($b64);
	}
	/**
	 * This sets the path to save all images in
	 * 
	 * @param String $path
	 * @throws ParameterException
	 */
	public function setPath($path)	{
		$path = realpath($path);
		if (!file_exists($path))	{
			throw new ParameterException(ERROR_IMAGE, "Your path is invalid", "400");
		}
		
		$this->images = $path . DIRECTORY_SEPARATOR . "images" ;
		$this->thumbs = $path . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "thumbnails" . DIRECTORY_SEPARATOR;
		
		if (!file_exists($this->images))	{
			mkdir($this->images);
		}
		if (!file_exists($this->thumbs))	{
			mkdir($this->thumbs);
		}
		if (!is_writable($this->images))	{
			throw new ParameterException(ERROR_IMAGE, $this->images . " folder is not writeable", "400");
		}
		if (!is_writable($this->thumbs))	{
			throw new ParameterException(ERROR_IMAGE, $this->thumbs . " folder is not writeable", "400");
		}
	}
	/**
	 * Takes an image, creates a thumbnail, and saves it to the images folder.
	 * Returns the filename of the created image.
	 * @return string
	 */
	public function processImage()	{
		$res = imagejpeg($this->fileData, realpath($this->images) . DIRECTORY_SEPARATOR . $this->fileName . '.jpg', 100);
		if (!$res)	{
			throw new BaseException(ERROR_IMAGE, "Unable to create image, please try again", "500");
		}
		if (!$this->makeThumbnail())	{
			throw new BaseException(ERROR_IMAGE, "Unable to create thumbnail, please try again", "500");
		}
		return $this->fileName . '.jpg';
	}
	/**
	 * Creates a thumbnail of the class's image
	 */
	private function makeThumbnail()	{
		$image = $this->fileData;
		$width = imagesx($image);
		$height = imagesy($image);
		$desired_height = floor($height * (THUMB_WIDTH / $width));
		
		$thumb = imagecreatetruecolor(THUMB_WIDTH, $desired_height);
		imagecopyresampled($thumb, $image, 0, 0, 0, 0, THUMB_WIDTH, $desired_height, $width, $height);
		return imagejpeg($thumb, realpath($this->thumbs) . DIRECTORY_SEPARATOR . $this->fileName . '.jpg');
	}
}
?>