<?php
namespace PenguinAPI\Models\DataMappers;

class IPTable	{
	
	private $db;
	
	public function __construct($db)	{
		$this->db = $db;
		date_default_timezone_set ( "America/Los_Angeles");
	}
	/**
	 * Checks if a user hasn't created a penguin too recently
	 * 
	 * @param unknown $ip
	 */
	public function ipCanCreate($ip, $updateTime)	{
		$this->db->addParam(":ip", $ip);
		$out = $this->db->query("SELECT time from " . DB_IP_LOG . " WHERE ip = :ip");
		
		//If we can't find the IP we assume it doesn't exist and create it. A "new" ip has no restrictions so is allowed to create a penguin.
		if ($out == false || count($out) == 0)	{
			$this->createIP($ip);
			return true;
		}
		$out = $out[0];
		$time = $out['time'];
		$curTime = time();
		$x = intval($curTime) - intval($time);
		if (intval($curTime) - intval($time) <= intval(PENGUIN_FLOOD_TIME))	{
			return false;
		}
		
		if ($updateTime)	{
			$this->updateTimestamp($ip);
		}
		
		return true;
	}
	/**
	 * Sets the timestamp to the specified value, or now if none is specified
	 * 
	 * @param $ip
	 * @param $time in date('Y-m-d H:i:s',time()) format
	 */
	public function updateTimestamp($ip, $time = null)	{
		//Convert current time to mysql timestamp format if not set
		if ($time == null)	{
			$time = time();
		}
		$this->db->addParam(":ip", $ip);
		$this->db->addParam(":time", $time);
		$this->db->query("UPDATE " . DB_IP_LOG . " SET time=:time WHERE ip=:ip");
	}
	/**
	 * 
	 * Checks if an ip hasn't already flagged penguin with id $id
	 * 
	 * $updateList can be set to update the flag list with the checked ID
	 * 
	 * @param PenguinId $id
	 * @param IP $ip
	 * @param bool $updateList
	 * @return boolean
	 */
	public function ipCanFlag($id, $ip, $updateList)	{
		//This uses transactions because we need to lock the rows to avoid race conditions and double flags
		$this->db->query("BEGIN");
		$list = $this->getFlagList($ip,$updateList);
		$ret = !isset($list[$id]);
		
		if ($updateList)	{
			$list[$id] = true;
			$this->updateFlagList($ip, $list);
		}
		$this->db->query("COMMIT");
		return $ret;
	}
	/**
	 * Updates the list of flagged images for an ip, either with a given list of by id
	 * 
	 * @param unknown $ip
	 * @param unknown $list
	 * @param number $id
	 */
	public function updateFlagList($ip, $list)	{
		$this->db->addParam(":ip", $ip);
		$this->db->addParam(":list", json_encode($list));
		$this->db->query("UPDATE " . DB_IP_LOG . " SET flagList = :list WHERE ip = :ip;");
	}
	/**
	 * Gets the flagged images list for a given ip
	 * @param unknown $ip
	 * @return boolean
	 */
	private function getFlagList($ip,$lock)	{
		//Lock decides whether or not we want to lock the database for the request.
		//We do when we are planning on updating it.
		$this->db->addParam(":ip", $ip);
		if ($lock)	{
			$q = "SELECT flagList from " . DB_IP_LOG . " where ip = :ip FOR UPDATE;";
		}
		else	{
			$q = "SELECT flagList from " . DB_IP_LOG . " where ip = :ip;";
		}	
		$out = $this->db->query($q);
		if ($out === array() || $out === false)	{
			$this->createIP($ip);
			return array();
		}
		return json_decode($out[0]['flagList'], TRUE);
	}
	
	/**
	 * Creates a record for an IP
	 * @param unknown $ip
	 */
	private function createIP($ip)	{
		$this->db->addParam(":ip", $ip);
		$this->db->addParam(":list", json_encode(array()));
		$this->db->addParam(":time", time());
		//Most of the values have sensible defaults so we don't need to add them
		$this->db->query("INSERT INTO " . DB_IP_LOG . "(ip, flagList, time) values (:ip,:list, :time);");
	}
}
?>