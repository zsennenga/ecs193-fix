<?php
namespace PenguinAPI\Models\DataMappers;

use PenguinAPI\Models\DB\DBWrapper;
use PenguinAPI\Exceptions\ParameterException;

class PenguinTable	{
	
	private $db;
	
	public function __construct(DBWrapper $db)	{
		$this->db = $db;	
	}
	
	/**
	 * Gets a penguin's data by ID
	 * @param unknown $id
	 * @throws \PenguinAPI\Exceptions\DBException
	 * @return unknown
	 */
	public function getPenguin($id)	{
		$this->db->query("BEGIN");
		$this->db->addParam(":id", $id);
		try	{
			$out = $this->db->query("SELECT id,image,name,views,time from " . DB_TABLE_PENGUIN . " where id = :id for update;");
		}
		catch (Exception $e)	{
			throw new \PenguinAPI\Exceptions\DBException(ERROR_DB, "Unable to Query Database", "404");
		}
		if (count($out) === 0 || $out === false)	{
			throw new \PenguinAPI\Exceptions\DBException(ERROR_DB, "Penguin id " . $id . " does not exist", "404");
		}
		$out = $out[0];
		$this->db->addParam(":id", $id);
		$this->db->query("UPDATE " . DB_TABLE_PENGUIN . " set views = views+1 where id = :id");
		$this->db->query("COMMIT");
		return $out;
	}
	/**
	 * Gets a list of random penguin ids
	 * 
	 * @param unknown $count
	 * @return unknown
	 */
	public function getRandomPenguins($count)	{
		$this->db->addParam(":count",$count);		
		//Dark magic from stackoverflow, it's a fast query to get random
		//rows in a large database without huge slowdowns/temp tables
		$out = $this->db->query("SELECT id, image, name, views, time
			FROM    (
        			SELECT  @cnt := COUNT(*) + 1,
                			@lim := :count
        			FROM    " . DB_TABLE_PENGUIN . "
        			) vars
			STRAIGHT_JOIN
        			(
        			SELECT  r.*,
                			@lim := @lim - 1
        			FROM    " . DB_TABLE_PENGUIN . " r
        			WHERE   (@cnt := @cnt - 1)
                			AND RAND() < @lim / @cnt
        			) i");
		return $out;
	}
	
	/**
	 * Gets penguins near a specified long/lat (Within 10km by default)
	 * 
	 * @param numeric $long
	 * @param numeric $lat
	 */
	public function getLocalPenguins($count, $lat, $long)	{
		$this->db->addParam(":lon1", $long);
		$this->db->addParam(":lon2", $long);
		$this->db->addParam(":lon3", $long);
		$this->db->addParam(":lon4", $long);

		$this->db->addParam(":lat1", $lat);
		$this->db->addParam(":lat2", $lat);
		
		$this->db->addParam(":lim", $count);
		//Dark magic from stackoverflow to calculate what points fit in a 10km region
		$out = $this->db->query("SELECT  id
   								 FROM    ".DB_TABLE_GEO."
   								 WHERE   MBRContains
                    			(
                    			LineString
                         		   (
                           			 Point
                                    	(
                                    		:lat1 + 10 / ( 111.1 / COS(RADIANS(:lon1))),
                                    		:lon2 + 10 / 111.1
                                   	 	)
								,
                           			 Point
                                    	(
                                    		:lat2 - 10 / ( 111.1 / COS(RADIANS(:lon3))),
                                    		:lon4 - 10 / 111.1
                                    	) 
                            	),
                    			pos
                    			)
								LIMIT :lim");
		$push = array();
		foreach ($out as $id)	{
			$this->db->addParam(":id", $id['id']);
			$temp = $this->db->query("SELECT id, image, name, views, time FROM " . DB_TABLE_PENGUIN . " WHERE id = :id");
			array_push($push, $temp[0]);
		}
		return $push;
	}
	/**
	 * Adds a penguin to the database, returns the id of the created penguin
	 * 
	 * @param String $image
	 * @param numeric $long
	 * @param numeric $lat
	 * @throws \PenguinAPI\Exceptions\DBException
	 * @return string
	 */
	public function addPenguin($image,$lat, $long, $name)	{
		$this->db->addParam(":image", $image);
		$this->db->addParam(":name", $name);
		try	{
			$this->db->query("INSERT INTO " . DB_TABLE_PENGUIN . " (image, name, views, flagCount) values (:image, :name, 0, 0)");
			$id = $this->db->lastInsertId('id');
			$this->db->addParam(":id", $id);
			//We use a special POINT data structure in the db to allow for easy lookups with getLocalPenguins
			$this->db->addParam(":pos", "POINT(" . $lat . " " . $long . ")");
			$this->db->query("INSERT INTO " . DB_TABLE_GEO . " (id, pos) values (:id, GeomFromText(:pos))");
		} catch (Exception $e)	{
			throw new PenguinAPI\Exceptions\DBException(ERROR_DB, "Unable to create penguin, error is " . $e->getMessage(), 500);
		}
		return $id;
	}
	/**
	 * Flags a penguin with a given id
	 * 
	 * @param unknown $id
	 */
	public function flagPenguin($id)	{
		$this->db->addParam(":id", $id);
		$out = $this->db->query("SELECT flagCount from " . DB_TABLE_PENGUIN . " WHERE id = :id FOR UPDATE");
		
		//This is sorta a catch-all if statement, probably not necessary but easier to check a few more conditions then
		//worry about errors later
		if ($out === array() || !isset($out[0]) || !isset($out[0]['flagCount']))	{
			throw new ParameterException(ERROR_PARAM, "Penguin with id " . $id . " not found", "404");
		}
		$out = $out[0];
		$this->db->addParam(":id", $id);
		$this->db->addParam(":fc", intval($out['flagCount'])+1);
		$this->db->query("UPDATE " . DB_TABLE_PENGUIN . " SET flagCount = :fc WHERE id = :id");
	}
	/**
	 * Gets the number of flags a penguin has
	 * 
	 * @param unknown $id
	 * @return int
	 */
	public function getFlags($id)	{
		$this->db->addParam(":id", $id);
		$out = $this->db->query("SELECT flagCount from " . DB_TABLE_PENGUIN . " WHERE id = :id");
	
		if ($out === false || $out === array())
			return 0;
		return $out[0]['flagCount'];
	}
}
?>