<?php

namespace PenguinAPI\Controllers;
/**
 * 
 * Controller for the Flag Route
 * 
 * @author its-zach
 */
class Flag extends BaseController	{

	public function POST()	{
		try	{
			$flagStatus = $this->serviceFactory->flagImage($this->requestParams['penguinId'], $this->requestParams['requestIP']);
		} catch (\PenguinAPI\Exceptions\BaseException $e) {
			$this->updateView($e->getMyMessage(), $e->getStatus() , $e->getResponseCode());
			return $e->getMessage();
		} catch (\Exception $e)	{
			$this->updateView($e->getMessage(), ERROR_GENERIC, 400);
			return $e->getMessage();
		}
		$this->updateView($flagStatus);
		return $flagStatus;
	}
}
?>