<?php

namespace PenguinAPI\Controllers;
use PenguinAPI\Exceptions\ParameterException;
/**
 * 
 * Controller for the Penguin Route
 * 
 * @author its-zach
 */
class Penguin extends BaseController	{

	public function GET()	{
		try	{
			
			if (isset($this->requestParams['penguinId']))	{
				$retVal = $this->serviceFactory->getPenguinById($this->requestParams['penguinId']);
			}
			
			else	{
				if (!isset($this->requestParams['count']))	{
					$this->updateView("Count is required for this request", ERROR_PARAM, 400);
					return "Count is required for this request";
				}
				
				//Set lat and long to null if not given as part of the request
				$lat = isset($this->requestParams['lat']) ? $this->requestParams['lat'] : null;
				$long = isset($this->requestParams['long']) ? $this->requestParams['long'] : null;
				
				$retVal = $this->serviceFactory->getPenguinIds($this->requestParams['count'], 
						$lat, 
						$long);
			}
			
		} catch (\PenguinAPI\Exceptions\BaseException $e) {
			$this->updateView($e->getMyMessage(), $e->getStatus() , $e->getResponseCode());
			return $e->getMessage();
				
		} catch (\Exception $e)	{
			//This code should only be called if something went wrong and 
			//I didn't check for something that explodes deeper in the code
			$this->updateView($e->getMessage(), ERROR_GENERIC, 400);
			return $e->getMessage();
		}
		$this->updateView($retVal);
		return $retVal;
	}
	
	public function POST()	{
		
		if (!isset($this->requestParams['lat']) ||
			!isset($this->requestParams['long']))	{
			$this->updateView("Lat and long are required for this request", ERROR_PARAM, 400);
			return "Lat and long are required for this request";
		}
		else if (!isset($this->requestParams['name']) || $this->requestParams['name'] === "")	{
			$this->updateView("Count is required for this request", ERROR_PARAM, 400);
			return "Name is required for this request";
		}
		
		if (!isset($this->requestParams['penguinImage']))	{
			$this->updateView("No image uploaded", ERROR_PARAM, 400);
			return "No image uploaded";
		}
		
		try	{
			$imagePath = $this->serviceFactory->saveImage($this->requestParams['penguinImage'], IMAGE_PATH);
			
			$id = $this->serviceFactory->makePenguin($imagePath, 
					$this->requestParams['lat'], 
					$this->requestParams['long'],
					$this->requestParams['requestIP'],
					$this->requestParams['name']);
			
		} catch (\PenguinAPI\Exceptions\BaseException $e) {
			$this->updateView($e->getMyMessage(), $e->getStatus() , $e->getResponseCode());
			return $e->getMessage();
			
		} catch (\Exception $e)	{
			//This code should only be called if something went wrong and
			//I didn't check for something that explodes deeper in the code
			$this->updateView($e->getMessage(), ERROR_GENERIC, 400);
			return $e->getMessage();
		}
		
		$this->updateView($id);
		return $id;
	}
}
?>