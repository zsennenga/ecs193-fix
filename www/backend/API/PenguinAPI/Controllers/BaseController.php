<?php

namespace PenguinAPI\Controllers;

use \PenguinAPI\Models\ServiceFactory;
/**
 * Basic Controller class
 * 
 * Handles view updating functions
 * @author its-zach
 * @codeCoverageIgnore
 *
 */
abstract class BaseController	{
	
	protected $serviceFactory;
	protected $requestParams;
	private $view;
	/**
	 * 
	 * Build the controller
	 * 
	 * @param view $view
	 * @param ServiceFactory $serviceFactory
	 */
	public final function __construct($view, $serviceFactory, $rp)	{
		$this->view = $view;
		$this->serviceFactory = $serviceFactory;
		$this->requestParams = $rp;
	}
	/**
	 * 
	 * Update the parameters of the current view.
	 * 
	 * @param string $message
	 * @param string $responseCode
	 * @param string $statusCode
	 */
	public function updateView($message = null, $statusCode = null, $responseCode = null)	{
		if (isset($message)) $this->view->setMessage($message);
		if (isset($responseCode)) $this->view->setResponse($responseCode);
		if (isset($statusCode)) $this->view->setStatus($statusCode);
	}
}