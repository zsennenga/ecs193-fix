<?php

namespace PenguinAPI\Controllers;
/**
 * 
 * Controller when no route is found
 * 
 * @author its-zach
 * @codeCoverageIgnore
 */
class NoRoute extends BaseController	{

	public function invoke() {
		$this->updateView("Route not Found", ERROR_PARAM, 404);
	}

}
?>