<?php

namespace PenguinAPI\Exceptions;
/**
 * Exception for PenguinAPI classes
 * 
 * We use pretty exceptions to make debugging and testing easier
 * They aren't functionally different, they just store a bit more info than
 * normal exceptions
 * 
 * @author its-zach
 * @codeCoverageIgnore
 */
class BaseException extends \Exception {

	protected $statusCode;
	protected $message;
	protected $responseCode;
	
	public function __construct($statusCode, $message, $responseCode)	{
		$this->statusCode = $statusCode;
		$this->message = $message;
		$this->responseCode = $responseCode;
	}
	
	public function getStatus()	{
		return $this->statusCode;
	}
	
	public function getMyMessage()	{
		return $this->message;
	}
	
	public function getResponseCode()	{
		return $this->responseCode;
	}

}
?>