<?php

use PenguinAPI\Utility\Log;
use PenguinAPI\Models\DB\DBWrapper;

require_once __DIR__ . "".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Data".DIRECTORY_SEPARATOR."Config.php";

class LogTest extends PHPUnit_Framework_TestCase {

	protected $object;
	protected $db;
	
	public function setUp()	{
		$this->db = new DBWrapper();
		$this->object = new Log($this->db, "asdf");
	}
	
	public function testLog()	{
		$this->object->setRoute('arr', 'blah');
		$this->object->setResp('res');
		$this->object->setParams(array());
		$this->object->execute();
		
		$db = $this->db;
		$out = $db->query("SELECT * FROM penguin_log WHERE ip = 'asdf'; ");
		$this->assertTrue($out != false);
		$out = $db->query("DELETE FROM penguin_log WHERE ip = 'asdf'; ");
	}

}
?>