<?php

include(__DIR__ . '/../../../vendor/autoload.php');
require_once __DIR__ . '/../Data/Config.php';


use PenguinAPI\Models\DB\DBWrapper;
use PenguinAPI\Controllers\Flag;
use PenguinAPI\Models\DomainObjectFactory;
use PenguinAPI\Models\DataMapperFactory;

class FlagTest extends \PHPUnit_Framework_TestCase	{
	
	protected $ip;
	protected $object;
	protected $view;
	protected $id;

	public function __construct()	{
		$db = new DBWrapper();
		$this->sf = new \PenguinAPI\Models\ServiceFactory(new DomainObjectFactory(),
				new DataMapperFactory($db));
		$this->ip = 'testContFlag';
		$log = $this->getMockBuilder("\PenguinAPI\Utility\Log")
					->disableOriginalConstructor()
					->getMock();
		
		$this->view = new \PenguinAPI\Views\Penguin($log, new \PenguinAPI\Templates\JSON());
	}
	
	public function setUp()	{
		$this->removeIP();
		$this->id = $this->sf->makePenguin('testContFlag', 1, 1, $this->ip, "steve");
	}
	public function tearDown()	{
		$this->removeIP();
		$this->removePenguin($this->id);
	}

	public function testCont()	{
		$rp = array();
		$rp['penguinId'] = $this->id;
		$rp['requestIP'] = $this->ip;
		$const = new Flag($this->view, $this->sf, $rp);
		$out = $const->POST();
		$this->assertTrue("" == $out);
		$out = $const->POST();
		$this->assertTrue($out == "IP has already flagged this image");
	}
	
	public function testNotNumeric()	{
		$rp = array();
		$rp['penguinId'] = "notnumeric";
		$rp['requestIP'] = $this->ip;
		$const = new Flag($this->view, $this->sf, $rp);
		$out = $const->POST();
		$this->assertTrue($out == "ID must be numeric");
	}
	
	public function testNotExist()	{
		$rp = array();
		$rp['penguinId'] = "5432";
		$rp['requestIP'] = $this->ip;
		$const = new Flag($this->view, $this->sf, $rp);
		$out = $const->POST();
		$this->assertTrue(strpos($out, "Penguin with id") !== false);
	
	}
	
	private function removePenguin($id)	{
		$db = new DBWrapper();
		$db->addParam(":id", $id);
		$db->query("DELETE from penguins where id = :id");
		$db->addParam(":id", $id);
		$db->query("DELETE from penguin_geo where id = :id");
		$db->query("ALTER TABLE penguins AUTO_INCREMENT = 1");
	}
	private function removeIP()	{
		$db = new DBWrapper();
		$db->addParam(":ip", $this->ip);
		$db->query("DELETE from penguin_ips where ip = :ip");
	}
	
}
?>