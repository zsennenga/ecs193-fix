<?php

include(__DIR__ . '/../../../vendor/autoload.php');
require_once __DIR__ . '/../Data/Config.php';


use PenguinAPI\Models\DB\DBWrapper;
use PenguinAPI\Controllers\Flag;
use PenguinAPI\Models\DomainObjectFactory;
use PenguinAPI\Models\DataMapperFactory;
use PenguinAPI\Controllers\Penguin;

class PenguinTest extends \PHPUnit_Framework_TestCase	{
	
	protected $ip;
	protected $object;
	protected $view;
	protected $id;
	protected $sf;

	public function __construct()	{
		$db = new DBWrapper();
		$this->sf = new \PenguinAPI\Models\ServiceFactory(new DomainObjectFactory(),
				new DataMapperFactory($db));
		$this->ip = 'testContFlag';
		$log = $this->getMockBuilder("\PenguinAPI\Utility\Log")
					->disableOriginalConstructor()
					->getMock();
		
		$this->view = new \PenguinAPI\Views\Penguin($log, new \PenguinAPI\Templates\JSON());
	}
	
	public function setUp()	{
		$this->id = $this->sf->makePenguin('testGet', 1, 1, $this->ip, "steve");
	}
	public function tearDown()	{
		$this->removeIP();
		$this->removePenguin($this->id);
	}

	public function testGet1()	{
		$rp = array();
		$rp['penguinId'] = $this->id;
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
		
		$out = $const->GET();
		
		$this->assertTrue($out['image'] == 'testGet');
	}
	
	public function testGet2()	{
		$rp = array();
		$rp['count'] = '10';
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
		
		$out = $const->GET();	
		$this->assertTrue(count($out) >= 1);
	}
	
	public function testGet3()	{
		$rp = array();
		$rp['count'] = '10';
		$rp['lat'] = 50;
		$rp['long'] = 50;
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
	
		$out = $const->GET();
		$this->assertSame(count($out),0);
		
		$rp = array();
		$rp['count'] = '10';
		$rp['lat'] = 1;
		$rp['long'] = 1;
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
		
		$out = $const->GET();
		$this->assertTrue(count($out)>=1);
	}
	
	public function testGet2Fail()	{
		$rp = array();
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
	
		$out = $const->GET();
		$this->assertSame($out, "Count is required for this request");
	}

	public function testGet3Fail()	{
		$rp = array();
		$rp['penguinId'] = '111111';
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
		
		$out = $const->GET();
		
		$this->assertSame($out, "Penguin id " . $rp['penguinId'] . " does not exist");
	}
	

	public function testNotNumeric()	{
		$rp = array();
		$rp['penguinId'] = 'notanumber';
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
	
		$out = $const->GET();
		
		$this->assertSame($out, "ID must be numeric");
	}
	
	public function testPost1()	{
		$rp = array();
		$const = new Penguin($this->view, $this->sf, $rp);
		$out = $const->POST();
		
		$this->assertSame("Lat and long are required for this request", $out);
	}
	
	public function testPostName()	{
		$rp = array();
		$rp['lat'] = 1;
		$rp['long'] = 2;
		$const = new Penguin($this->view, $this->sf, $rp);
		$out = $const->POST();
	
		$this->assertSame("Name is required for this request", $out);
	}
	
	public function testPost2()	{
		$rp = array();
		$rp['lat'] = 1;
		$rp['long'] = 2;
		$rp['name'] = "steve";
		$const = new Penguin($this->view, $this->sf, $rp);
		$out = $const->POST();
	
		$this->assertSame("No image uploaded", $out);
	}
	
	public function testPost3()	{
		$rp = array();
		$rp['lat'] = 1;
		$rp['long'] = 2;
		$rp['name'] = "steve";
		$rp['penguinImage'] = "notanimage";
		$const = new Penguin($this->view, $this->sf, $rp);
		$out = $const->POST();
		$this->assertSame("Uploaded image is invalid", $out);
	}
	
	public function testPost5()	{
		$this->removeIP();
		$rp = array();
		$rp['lat'] = 1;
		$rp['long'] = 2;
		$rp['name'] = "steve";
		$f = __DIR__ . "".DIRECTORY_SEPARATOR."..".DIRECTORY_SEPARATOR."Data".DIRECTORY_SEPARATOR."testPenguin.jpg";
		$rp['penguinImage'] = 'data:image/jpeg;base64,' . base64_encode(file_get_contents($f));
		$rp['requestIP'] = $this->ip;
		$const = new Penguin($this->view, $this->sf, $rp);
		$out = $const->POST();
		$this->assertTrue(is_numeric($out));
		$this->removePenguin($out);
		$out = $const->POST();
		$this->assertSame("You are creating Penguins too fast", $out);
		$path = IMAGE_PATH ."images";
		$path = realpath($path);
		$this->removePenguin($out);
	    $this->rrmdir($path);  
	}
	
	
	private function removePenguin($id)	{
		$db = new DBWrapper();
		$db->addParam(":id", $id);
		$db->query("DELETE from penguins where id = :id");
		$db->addParam(":id", $id);
		$db->query("DELETE from penguin_geo where id = :id");
		$db->query("ALTER TABLE penguins AUTO_INCREMENT = 1");
	}
	private function removeIP()	{
		$db = new DBWrapper();
		$db->addParam(":ip", $this->ip);
		$db->query("DELETE from penguin_ips where ip = :ip");
	}
	
	private function rrmdir($dir) { 
	   if (is_dir($dir)) { 
	     $objects = scandir($dir); 
	     foreach ($objects as $object) { 
	       if ($object != "." && $object != "..") { 
	         if (filetype($dir."/".$object) == "dir") $this->rrmdir($dir."/".$object); 
	         else unlink($dir."/".$object); 
	       } 
	     } 
	     reset($objects); 
	     rmdir($dir); 
	   } 
 } 
	
}
?>