<?php

use PenguinAPI\Models\DB\DBWrapper;
include(__DIR__ . '/../../../../vendor/autoload.php');
require_once __DIR__ . '/../../Data/Config.php';

class PenguinTableTest extends \PHPUnit_Framework_TestCase	{

	protected $object;
	protected $ip;
	
	public function setUp()	{
		$this->db = new DBWrapper();
		$this->object = new \PenguinAPI\Models\DataMappers\PenguinTable($this->db);
		$this->ip = 'test1';
		$this->db->query("ALTER TABLE penguins AUTO_INCREMENT = 1");
	}
	
	public function testAddPenguin()	{
		$id = $this->object->addPenguin('testAdd.jpg', '123', '456', 'bill');
		
		$this->db->addParam(":id", $id);
		$out = $this->db->query("SELECT * from penguins WHERE id = :id;");
		$this->assertTrue($out != false);
		$this->assertSame('testAdd.jpg', $out[0]['image']);
		$this->assertSame('bill', $out[0]['name']);
		$this->removePenguin($id);
	}
	/**
	 * @expectedException \Exception
	 */
	public function testGetPenguin()	{
		$id = $this->object->addPenguin('testGet.jpg', '1234', '4567', "frank");
		$out = $this->object->getPenguin($id);
		$this->assertTrue($out != false);
		$this->assertSame('testGet.jpg', $out['image']);
		
		$this->removePenguin($id);
		$this->object->getPenguin($id);
	}
	
	public function testLocalPenguins()	{
		
		$id1 = $this->object->addPenguin("testLocal1", "38" , "-121", "frank");
		$id2 = $this->object->addPenguin("testLocal2", "38", "-121", "frank");
		$id3 = $this->object->addPenguin("testLocal3", "38", "-121", "frank");
		$id4 = $this->object->addPenguin("testLocal4", "-5.01", "0", "frank");
		$id5 = $this->object->addPenguin("testLocal5", "-5.01", "0", "frank");
		
		$out = $this->object->getLocalPenguins(100, "38", "-121");
		$this->assertSame(count($out),3);
		$out = $this->object->getLocalPenguins(100, "-5", "0");
		$this->assertSame(count($out),2);
		$out = $this->object->getLocalPenguins(100, "50", "50");
		$this->assertTrue(empty($out));
		
		$this->removePenguin($id1);
		$this->removePenguin($id2);
		$this->removePenguin($id3);
		$this->removePenguin($id4);
		$this->removePenguin($id5);
	}
	
	
	public function testGetRandomPenguins()	{
		$ids = array();
		
		for ($i = 0; $i < 100; $i++)	{
			array_push($ids, $this->object->addPenguin('testRandom', 1, 2, "frank"));
		}
		
		$out = $this->object->getRandomPenguins(40);
		
		$this->assertSame(40, count($out));
		foreach($out as $id)	{
			$var = $this->object->getPenguin($id['id']);
			$this->assertInternalType('array', $var);
			$this->assertArrayHasKey('image',$var);
			$this->assertArrayHasKey('id',$var);
			$this->assertTrue($var['image'] != null);
			$this->assertTrue($var['id'] == $id['id']);
		}
		
		foreach ($ids as $id)	{
			$this->removePenguin($id);
		}
		
	}

	public function testFlagPenguin()	{
		$id = $this->object->addPenguin('testFlag.jpg', '123', '456', "frank");
		$this->object->flagPenguin($id);
		$this->object->flagPenguin($id);
		$out = $this->object->getFlags($id);
		$this->assertSame(2,$out);
		$this->removePenguin($id);
	}
	/**
	 * @expectedException \Exception
	 */
	public function testFlagExcept()	{
		$this->object->flagPenguin('111111');
	}

	private function removePenguin($id)	{
		$db = new DBWrapper();
		$db->addParam(":id", $id);
		$db->query("DELETE from penguins where id = :id");
		$db->addParam(":id", $id);
		$db->query("DELETE from penguin_geo where id = :id");
		$db->query("ALTER TABLE penguins AUTO_INCREMENT = 1");
	}
}
?>