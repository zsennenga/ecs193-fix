<?php

use PenguinAPI\Models\DB\DBWrapper;
include(__DIR__ . '/../../../../vendor/autoload.php');
require_once __DIR__ . '/../../Data/Config.php';

class IPTableTest extends \PHPUnit_Framework_TestCase	{

	protected $object;
	protected $ip;
	
	public function setUp()	{
		$this->object = new \PenguinAPI\Models\DataMappers\IPTable(new DBWrapper());
		$this->ip = 'test1';
	}

	public function testCanFlag()	{
		$this->removeIP();
		$this->assertTrue($this->object->ipCanFlag(1, $this->ip, false));
		$this->object->ipCanFlag(1, $this->ip, true);
		$this->assertFalse($this->object->ipCanFlag(1, $this->ip, false));
		
		$this->assertTrue($this->object->ipCanFlag(2, $this->ip, true));
		$this->assertTrue($this->object->ipCanFlag(3, $this->ip, true));
		$this->assertTrue($this->object->ipCanFlag(4, $this->ip, true));
		$this->assertFalse($this->object->ipCanFlag(2, $this->ip, false));
		$this->assertFalse($this->object->ipCanFlag(3, $this->ip, false));
		$this->assertFalse($this->object->ipCanFlag(4, $this->ip, false));
		$this->assertFalse($this->object->ipCanFlag(1, $this->ip, false));
		$this->removeIP();
	}
	
	public function testTimeStamp()	{
		$this->removeIP();
		$this->object->ipCanCreate($this->ip, false);
		$time = time()+4000;
		$this->object->updateTimestamp($this->ip, $time);
		$db = new DBWrapper();
		$db->addParam(":ip", $this->ip);
		$out = $db->query("SELECT time FROM " . DB_IP_LOG . " WHERE ip = :ip");
		$out = $out[0];
		$this->assertTrue(strtotime($time) == strtotime($out['time']));
		$this->removeIP();
	}
	
	public function testCanCreate()	{
		$this->removeIP();
		$this->assertTrue($this->object->ipCanCreate($this->ip, false));
		$this->assertFalse($this->object->ipCanCreate($this->ip, false));
		$time = intval(time())-11000;
		$this->object->updateTimestamp($this->ip, date('Y-m-d H:i:s',$time));
		$this->assertTrue($this->object->ipCanCreate($this->ip, false));
		$this->assertTrue($this->object->ipCanCreate($this->ip, true));
		$this->assertFalse($this->object->ipCanCreate($this->ip, true));
		$this->assertFalse($this->object->ipCanCreate($this->ip, true));
		
		$this->removeIP();
	}
	
	private function removeIP()	{
		$db = new DBWrapper();
		$db->addParam(":ip", $this->ip);
		$db->query("DELETE from penguin_ips where ip = :ip");
	}
}
?>