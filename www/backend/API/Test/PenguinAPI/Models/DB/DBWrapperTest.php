<?php

include(__DIR__ . '/../../../../vendor/autoload.php');
require_once __DIR__ . '/../../Data/Config.php';

class DBWrapperTest extends \PHPUnit_Framework_TestCase	{

	protected $object;
	
	public function setUp()	{
		$this->object = new PenguinAPI\Models\DB\DBWrapper();
	}
	/**
     * @expectedException PenguinAPI\Exceptions\DBException
     */
	public function testPrepareError()	{
		$this->object->query("Not sql");
	}
	
	public function testBindExecError()	{
		$out = $this->object->query("SELECT * FROM penguins WHERE id = :id");
		
		$this->assertSame(array(),$out);
	}

}
?>