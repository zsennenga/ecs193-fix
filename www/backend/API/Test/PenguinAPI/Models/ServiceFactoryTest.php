<?php

include(__DIR__ . '/../../../vendor/autoload.php');
require_once __DIR__ . '/../Data/Config.php';


use PenguinAPI\Models\DB\DBWrapper;
use PenguinAPI\Models\DomainObjectFactory;
use PenguinAPI\Models\DataMapperFactory;

class ServiceFactoryTest extends \PHPUnit_Framework_TestCase	{
	
	protected $object;
	protected $ip;

	public function __construct()	{
		$this->db = new DBWrapper();
		$this->object = new \PenguinAPI\Models\ServiceFactory(new DomainObjectFactory(),
															  new DataMapperFactory($this->db));
		$this->ip = 'test1';
		$this->db->query("ALTER TABLE penguins AUTO_INCREMENT = 1");
	}
	public function testAddImage()	{
		$path = dirname(__FILE__);
		$images = $path . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR;
		$thumbs = $images . "thumbnails" . DIRECTORY_SEPARATOR;
		$testJpg = __DIR__ . '/../Data/testPenguin.jpg';
		$hashSmall = md5_file(__DIR__ . '/../Data/testThumb.jpg');
		$hs2 = 'c21bc9219c82fac357e74eb880f84342';
		$f = file_get_contents($testJpg);
		$hashBig = md5($f);
		$testJpg = base64_encode($f);
		$out = $this->object->saveImage($testJpg, $path);
		$this->assertSame($hashBig . ".jpg", $out);
		$this->assertFileExists($thumbs . $hashBig . ".jpg");
		$this->assertFileExists($images . $hashBig . ".jpg");
		$testThumbHash = md5_file($thumbs . $hashBig . ".jpg");
		$this->assertTrue($hashSmall == $testThumbHash || $hs2 == $testThumbHash);
		$testImgHash = md5_file($images . $hashBig . ".jpg");
		$hI2 = '535d00afaeb1ac58da823e21400171b8';
		$this->assertTrue("7341d1ec6074806bedd66e9f23c41984" == $testImgHash || $hI2 == $testImgHash);
		unlink($thumbs . $hashBig . ".jpg");
		unlink($images . $hashBig . ".jpg");
		rmdir($thumbs);
		rmdir($images);
	}
	
	
	public function testMakePenguin()	{
		$this->removeIP();
		$id = $this->object->makePenguin('testServiceAdd', 0, 0, $this->ip, "frank");
		
		$this->db->addParam(":id", $id);
		$out = $this->db->query("SELECT * from penguins WHERE id = :id;");
		$this->assertTrue($out != false);
		$this->assertSame('testServiceAdd', $out[0]['image']);
		$this->assertSame('frank', $out[0]['name']);
		$this->removePenguin($id);
		
		
	}
	/**
	 * @expectedException \Exception
	 */
	public function testMakeExcept()	{
		$id = $this->object->makePenguin('testServiceAdd', 0, 0, $this->ip, "steve");
	}
	
	/**
	 * @expectedException \Exception
	 */
	public function testFlood()	{
		$this->removeIP();
		$id = $this->object->makePenguin('testServiceAdd', 0, 0, $this->ip, "steve");
		$this->removePenguin($id);
		$id = $this->object->makePenguin('testServiceAdd', 0, 0, $this->ip, "steve");
	}
	/**
	 * @expectedException \Exception
	 */
	public function testGetPenguinbyId()	{
		$this->removeIP();
		$id = $this->object->makePenguin('testServiceGetId', 0, 0, $this->ip, "steve");
		
		$out = $this->object->getPenguinById($id);
		$this->assertTrue($out != false);
		$this->assertSame('testServiceGetId', $out['image']);
		
		$this->removePenguin($id);
		
		$this->object->getPenguinById("not numeric");
	}
	public function testFlagImage()	{
		$this->removeIP();
		$id = $this->object->makePenguin('testFlagImage', 0, 0, $this->ip, "steve");
		
		$this->db->addParam(":id", $id);
		$out = $this->db->query("SELECT * from penguins WHERE id = :id");
		$this->assertSame(0, $out[0]['flagCount']);
		$this->object->flagImage($id, $this->ip);
		
		$this->db->addParam(":id", $id);
		$out = $this->db->query("SELECT * from penguins WHERE id = :id");
		$this->assertSame(1, $out[0]['flagCount']);
		
		$this->db->addParam(":ip", $this->ip);
		$out = $this->db->query("SELECT flagList from " . DB_IP_LOG . " where ip = :ip");
		$out = json_decode($out[0]['flagList'], true);
		$this->assertTrue($out[$id]);
		
		$this->removePenguin($id);
		$this->removeIP();
	
	}
	
	/**
	 * @expectedException \Exception
	 */
	public function testExcept()	{
		$this->object->flagImage('notNumeric', $this->ip);
	}
	
	/**
	 * @expectedException \Exception
	 */
	public function testDoubleFlag()	{
		$this->removeIP();
		$id = $this->object->makePenguin('testFlagImage', 0, 0, $this->ip, "steve");
		$this->object->flagImage($id, $this->ip);
		$this->removePenguin($id);
		$this->removeIP();
		$this->object->flagImage($id, $this->ip);
		$this->removePenguin($id);
	}
	
	/**
	 * @expectedException \Exception
	 */
	public function testCheckLongLat()	{
		$this->removeIP();
		$this->object->makePenguin('testFail', "notnumeric", "notnumeric", $this->ip, "steve");
		
	}
	/**
	 * @expectedException \Exception
	 */
	public function testCheckLongLat2()	{
		$this->removeIP();
		$this->object->makePenguin('testFail', null, null, $this->ip, "steve");
	}
	
	public function testPenguinIds()	{
		$ids = array();
		$this->removeIP();
		for ($i = 0; $i < 100; $i++)	{
			array_push($ids, $this->object->makePenguin('testServiceIds', 1, 2, $this->ip, "steve"));
			$this->removeIP();
		}
		
		$out = $this->object->getPenguinIds(40);
		
		$this->assertSame(40, count($out));
		
		$out = $this->object->getPenguinIds(40, 1, 2);
		
		$this->assertSame(40, count($out));
		
		foreach($out as $id)	{
			$var = $this->object->getPenguinById($id['id']);
			$this->assertInternalType('array', $var);
			$this->assertArrayHasKey('image',$var);
			$this->assertArrayHasKey('id',$var);
			$this->assertTrue($var['image'] != null);
			$this->assertTrue($var['id'] == $id['id']);
		}
		
		$out = $this->object->getPenguinIds(1000);
		$this->assertSame(10, count($out));
		
		foreach ($ids as $id)	{
			$this->removePenguin($id);
		}
		$this->removeIP();
	}
	
	
	
	private function removePenguin($id)	{
		$db = new DBWrapper();
		$db->addParam(":id", $id);
		$db->query("DELETE from penguins where id = :id");
		$db->addParam(":id", $id);
		$db->query("DELETE from penguin_geo where id = :id");
		$db->query("ALTER TABLE penguins AUTO_INCREMENT = 1");
	}
	private function removeIP()	{
		$db = new DBWrapper();
		$db->addParam(":ip", $this->ip);
		$db->query("DELETE from penguin_ips where ip = :ip");
	}
}
?>