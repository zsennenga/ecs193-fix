<?php
define("DB_HOST", "localhost");
define("DB_NAME", "penguin_rest");
define("DB_TABLE_PENGUIN", "penguins");
define("DB_TABLE_LOG", "penguin_log");
define("DB_IP_LOG", "penguin_ips");
define("DB_TABLE_GEO", "penguin_geo");
define("DB_USER", "root");
define("DB_PASS", "");
define("DEFAULT_FLAG_THRESHOLD", 5);
define("PENGUIN_FLOOD_TIME", 300);
define("THUMB_WIDTH", 125);
define("IMAGE_PATH", "EDITME");
define("IMAGE_URL","EDITME");
define("ROUTE_PREFIX", "");

// DO NOT EDIT BELOW THIS LINE
define("ERROR_GENERIC", "-1");
define("ERROR_DB", "-2");
define("ERROR_FILE", "-3");
define("ERROR_PARAM", "-4");
define("ERROR_IMAGE", "-5");
define("ERROR_IP", "-6");
?>