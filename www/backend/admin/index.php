<?php

require_once('../API/PenguinAPI/Models/DB/DBWrapper.php');
require_once('../Config.php');
require_once('./Utility/Helper.php');
require_once('./Utility/Template.php');

echo templateHeader();
//If someone posted something to us, do something about it.
if (isset($_POST['action']) && isset($_POST['id']))	{
	$res = doAction($_POST['action'], $_POST['id']);
	templateAction($_POST['action'], $_POST['id'], $res);
}

//Initialize variables
$db = new DBWrapper();
$fCount = DEFAULT_FLAG_THRESHOLD;
//The threshold for how flagged a penguin needs to be to be displayed is set here
if (isset($_GET['fCount']))	
	$fCount = $_GET['fCount'];

//Get the penguins who's flagCounts meet the threshold
$out = getFlaggedPenguins($fCount);

//If we didn't find any, great, we're done (almost)
if (count($out) == 0)	{
	echo templateNone();
}
else	{
	//Otherwise, we begin the table/form
	echo templateStart();
	foreach ($out as $peng)	{
		echo templateItem($peng);
	}
	echo templateEnd();
}


echo templateFooter();
?>