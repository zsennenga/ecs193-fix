<?php

function getFlaggedPenguins($fCount)	{
	$db->addParam(':fc', $fCount);
	$out = $db->query('SELECT id, image, flagCount from ' . DB_TABLE_PENGUIN . 
			' where flagCount >= :fc ORDER BY flagCount DESC');
}

function doAction($action, $id)	{
	if ($action === "Delete")	{
		$o = deletePenguin($id);
		if ($o === false)
			return false;
		return true;
	}
	else if ($action === "Clear")	{
		$o = clearFlags($id);
		if ($o === false)
			return false;
		return true;
	}
	else
		return false;
}

function deletePenguin($id)	{
	$db = new DBWrapper();
	$db->addParam(':id', $id);
	return $db->query('DELETE FROM ' . DB_TABLE_PENGUIN . ' WHERE id = :id');	
}

function clearFlags($id)	{
	$db = new DBWrapper();
	$db->addParam(':id', $id);
	return $db->query('UPDATE ' . DB_TABLE_PENGUIN . 'SET flagCount = 0 WHERE id = :id');
}

function handleAction($action, $id)	{
	switch($action)	{
		case 'CLEAR':
			clearFlags($id);
			break;
		case 'DELETE':
			deletePenguin($id);
			break;
		default:
			break;
	}	
}
?>