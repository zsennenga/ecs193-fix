<?php
require_once('./Utility/Helper.php');
function templateAction($action, $id)	{
	if ($res)	{
		return file_get_contents('../html/templateActionSuccess.html');
	}
	return file_get_contents('../html/templateActionFailure.html');
}

function templateHeader()	{
	return file_get_contents('../html/header.html');
}

function templateNone()	{
	return file_get_contents('../html/templateNone.html');
}

function templateStart()	{
	return file_get_contents('../html/templateStart.html');
}

function templateItem($peng)	{
	$row = file_get_contents('../html/templateRow.html');
	$row = str_replace(":ID", $peng['id'], $row);
	$row = str_replace(":FLAGS", $peng['flagCount'], $row);
	$url = IMAGE_URL . $peng['image'];
	$row = str_replace(":URL", $url, $row);
	return $row;
}

function templateEnd()	{
	return file_get_contents('../html/templateEnd.html');
}

function templateFooter()	{
	return file_get_contents('../html/footer.html');
}

?>