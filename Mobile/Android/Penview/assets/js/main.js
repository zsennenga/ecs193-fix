
/* Responsive HTML5 gallery tutorial
 *
 * http://tomicloud.com/2014/01/responsive-gallery
 * https://github.com/tomimick/responsive-image-gallery */

$(function() {
    /*--------------------------------------------------------------------------*/
    // canvas implementation
        //declaring variables
		
		//MACROS
		var trailingBlur = 10;
		var waterColorBlur = 20;
		var topBarHeight = 60;	//This was causing the offset, the top bar was not accounted for in getting event Y coordinate
		var aspectWidth = 9;
		var aspectHeight = 16;

		var canW = 0;
		var canH = 0;
		

		
        var oCanvas = document.getElementById("thecanvas");
        var oCtx = oCanvas.getContext('2d');
				
				
				
		canW = window.innerWidth;
		canH = window.innerHeight  - 60 - 25 - 58 - 58 - 37;	//have to look into getting this magic numbers dynamically
																			//60: Menu bar height
																			//25: black bar between canvas and tools
																			//58: Tools icon height, 1nd row
																			//58: Tools icon height, 2st row
																			//37: there was still some fluff
	
		console.log(window.innerHeight);
		console.log(aspectHeight);
		console.log(Math.floor(canH / aspectHeight));
		
		console.log(window.innerWidth);
		console.log(aspectWidth);
		console.log(Math.floor(canW / aspectWidth));
		
		if(canH/aspectHeight <= canW/aspectWidth){
			oCtx.canvas.height = Math.floor(canH / aspectHeight) * aspectHeight;
			oCtx.canvas.width = Math.floor(canH / aspectHeight) * aspectWidth;
			console.log("h <= W")
			}
		else{
			oCtx.canvas.width = Math.floor(canW / aspectWidth) * aspectWidth;
			oCtx.canvas.height = Math.floor(canW / aspectWidth) * aspectHeight;
		}		
		
		console.log(oCtx.canvas.width);
		console.log(oCtx.canvas.height);
		
		var clickX = new Array();
        var clickY = new Array();
        var clickDrag = new Array();
        var lineWidths = new Array();
        var shadowBlurs = new Array();
        var bMouseIsDown = false; //mouse is down
		var bCanvasClean = true; //To not loop the fade.
        var paintbrush_type = 0;  //type of paintbrush
        var imageGalleryInit = 0;
        var stroke_img = new Image();
        ////stroke_img.src = 'stroke.png';
        //'http://www.tricedesigns.com/wp-content/uploads/2012/01/brush2.png';
        //stroke_img.crossOrigin = 'anonymous';

        var lastPoint;//used for stroke_paintbrush
        var timeInterval;//used for penguinFade()
        var iWidth = oCanvas.width;
        var iHeight = oCanvas.height;
        var lineWidth = 30;
		

        var shadowBlur = 0;
        var curr_count = 0;
        var local_search = 0;

        //setting up initial parameters
        oCtx.fillStyle = "rgb(255,255,255)";
        oCtx.fillRect(0,0,iWidth,iHeight);
		
		
		oCtx.beginPath();
        
		
		oCtx.strokeStyle = "rgb(0,0,0)";
        oCtx.lineJoin = oCtx.lineCap = 'round';
        oCtx.lineWidth = lineWidth;
        oCtx.shadowBlur = shadowBlur;
        oCtx.shadowColor = 'rgb(0, 0, 0)';

		
		window.addEventListener('resize', resizeCanvas, false);
		
		oCanvas.addEventListener("mousedown", mouseDown);
		oCanvas.addEventListener("mousemove", mouseMove);
		oCanvas.addEventListener("mouseup", mouseUp);
		oCanvas.addEventListener("mouseout", mouseOut);
		//oCanvas.addEventListener("mouseover", mouseOver);
		//oCanvas.addEventListener("mouseout", touchCancel);
		oCanvas.addEventListener("touchstart", touchDown);
		oCanvas.addEventListener("touchmove", touchMove);
		oCanvas.addEventListener("touchend", touchUp);
		oCanvas.addEventListener("touchcancel", touchCancel);
		
        // check if we have canvas support
        //var bHasCanvas = false;
        // if (oCanvas.getContext("2d")) {
        //  bHasCanvas = true;
        //  oCtx = oCanvas.getContext('2d');
        // }

        // // no canvas, bail out.
        // if (!bHasCanvas) {
        //  return {
        //      saveAsBMP : function(){},
        //      saveAsPNG : function(){},
        //      saveAsJPEG : function(){}
        //  }
        // }

    //     //used to determine distance between two points
    //     //this is used for stroke_paintbrush
    //     function distanceBetween(point1, point2){
    //         return Math.sqrt(Math.pow(point2.x - point1.x, 2) + 
    //             Math.pow(point2.y - point1.y, 2));
    //     }

    //     //used to determine the angle between two points
    //     //this is used for stroke_paintbrush
    //     function angleBetween(point1, point2){
    //         return Math.atan2(point2.x - point1.x, point2.y-point1.y);
    //     }

        //selecting the shadow to create the watercolor paint effect
        function selectShadow (id_of_paintbrush){
            if(paintbrush_type == 0)
                shadowBlur = 0;
            else if(paintbrush_type == 1)
                shadowBlur = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * waterColorBlur;
            else if(paintbrush_type == 2)
                shadowBlur = 0; 

            var elems = document.getElementsByTagName('*'), i;
            for (i in elems) {
                if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrush' + ' ')
                        > -1) {
                    if(elems[i].getAttribute('id') == id_of_paintbrush){
                        elems[i].setAttribute("style", "opacity: 1; display: inline; border: solid #666699 3px;");
                    }else{
                        elems[i].setAttribute("style", "opacity: 0.6; display: inline; border: solid #666699 3px;");
                    }
                }
            }
        }

        //selecting the size of the paintbrush
        function selectSize(id_of_paintbrushSize){
            var elems = document.getElementsByTagName('*'), i;
            for (i in elems) {
                if((' ' + elems[i].className + ' ').indexOf(' ' + 'paintbrushSize' + ' ')
                        > -1) {
                    if(elems[i].getAttribute('id') == id_of_paintbrushSize){
                        elems[i].setAttribute("style", "opacity: 1; display: inline;");
                    }else{
                        elems[i].setAttribute("style", "opacity: 0.6; display: inline");
                    }
                }
            }
        }

		function resizeCanvas(){
			canW = window.innerWidth;
			canH = window.innerHeight  - 60 - 25 - 58 - 58 - 37;	//have to look into getting this magic numbers dynamically
																				//60: Menu bar height
																				//25: black bar between canvas and tools
																				//58: Tools icon height, 1nd row
																				//58: Tools icon height, 2st row
																				//37: there was still some fluff
					
			if(canH/aspectHeight <= canW/aspectWidth){
				oCtx.canvas.height = Math.floor(canH / aspectHeight) * aspectHeight;
				oCtx.canvas.width = Math.floor(canH / aspectHeight) * aspectWidth;
				console.log("h <= W")
				}
			else{
				oCtx.canvas.width = Math.floor(canW / aspectWidth) * aspectWidth;
				oCtx.canvas.height = Math.floor(canW / aspectWidth) * aspectHeight;
			}		
            clearCanvas();
				
			clickX = new Array();
			clickY = new Array();
			clickDrag = new Array();
			lineWidths = new Array();
			shadowBlurs = new Array();
			bMouseIsDown = false; //mouse is down
			bCanvasClean = true; //To not loop the fade.
			paintbrush_type = 0;  //type of paintbrush
			imageGalleryInit = 0;
			stroke_img = new Image();
			////stroke_img.src = 'stroke.png';
			//'http://www.tricedesigns.com/wp-content/uploads/2012/01/brush2.png';
			//stroke_img.crossOrigin = 'anonymous';

			lastPoint;//used for stroke_paintbrush
			timeInterval;//used for penguinFade()
			iWidth = oCanvas.width;
			iHeight = oCanvas.height;
			lineWidth = 30;
			

			shadowBlur = 0;
			curr_count = 0;
			local_search = 0;

			//setting up initial parameters
			oCtx.fillStyle = "rgb(255,255,255)";
			oCtx.fillRect(0,0,iWidth,iHeight);
			
			
			oCtx.beginPath();
			
			
			oCtx.strokeStyle = "rgb(0,0,0)";
			oCtx.lineJoin = oCtx.lineCap = 'round';
			oCtx.lineWidth = lineWidth;
			oCtx.shadowBlur = shadowBlur;
			oCtx.shadowColor = 'rgb(0, 0, 0)';
			
        }
		

		//When touch begins
		function touchDown(e){
			e.preventDefault();
			bCanvasClean = false;
			bMouseIsDown = true;
			
			iLastX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
			iLastY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
	        //iLastX = e.offsetX || e.layerX;
            //iLastY = e.offsetY || e.layerY;
            addClick(iLastX, iLastY);
            redraw();
			lastPoint = {x: iLastX, y: iLastY};
		}
		
		
        //when the mouse is up
        function mouseUp() {
            bMouseIsDown = false;
        }

		//When the finger is lifted
		function touchUp(e){
			bMouseIsDown = false;
		}

        //when the mouse is down
        function mouseDown(e) {
			bCanvasClean = false;
            bMouseIsDown = true;
            //iLastX = e.offsetX || e.layerX;
            //iLastY = e.offsetY || e.layerY;
			iLastX = e.clientX - oCanvas.offsetLeft + document.body.scrollLeft;
			iLastY = e.clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
            addClick(iLastX, iLastY);
            redraw();
            //oCtx.moveTo(iLastX, iLastY);
            lastPoint = {x: iLastX, y: iLastY};
		}

		
        //if the mouse is down and being dragged
        function mouseMove(e) {
            if (bMouseIsDown) {
                //var iX = e.offsetX || e.layerX;
                //var iY = e.offsetY || e.layerY;
                
				var iX = e.clientX - oCanvas.offsetLeft + document.body.scrollLeft;
				var iY = e.clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;
				
				// oCtx.lineTo(iX, iY);
                // oCtx.stroke();
                // iLastX = iX;
                // iLastY = iY;
                addClick(iX, iY, true);
                redraw();
            }
        }

		function touchMove(e){
			e.preventDefault();
			if (bMouseIsDown) {
			var iX = e.touches[0].clientX - oCanvas.offsetLeft + document.body.scrollLeft;
			var iY = e.touches[0].clientY - oCanvas.offsetTop + document.body.scrollTop - topBarHeight;

			addClick(iX, iY, true);
			redraw();
			}
		}
		
        function mouseOut(e) {
            bMouseIsDown = false;
        }

		function touchCancel(e) {
			bMouseIdDown = false;
		}
		
        //add these to each of the arrays
        function addClick(x, y, dragging)
        {
          clickX.push(x);
          clickY.push(y);
          clickDrag.push(dragging);
          lineWidths.push(lineWidth);
          shadowBlurs.push(shadowBlur);
        }

        //redrawing the canvas with the elements in the arrays
        function redraw(){	
			var offSet = curr_count;
			if(curr_count > trailingBlur)
				offSet = offSet - trailingBlur;
			for(var i=offSet; i < clickX.length; i++) {
				//var i = curr_count;
				oCtx.lineWidth = lineWidths[i]; 
				oCtx.shadowBlur = shadowBlurs[i];   
				oCtx.beginPath();
				if(clickDrag[i] && i){
				  oCtx.moveTo(clickX[i-1], clickY[i-1]);
				 }else{
				   oCtx.moveTo(clickX[i]-1, clickY[i]);
				 }
				 oCtx.lineTo(clickX[i], clickY[i]);
				 oCtx.closePath();
				 oCtx.stroke();
				 curr_count++;
			  }
        }

        //clearing the canvas
        function clearCanvas(){
			bCanvasClean = true;
            oCtx.fillStyle = "rgb(255,255,255)";
            oCtx.fillRect(0,0,oCanvas.width,oCanvas.height);
            clickX = [];
            clickY = [];
            clickDrag = [];
            lineWidths = [];
            shadowBlurs = [];
            curr_count = 0;
			document.getElementById('thecanvas').style.opacity = 1;
        }

    //     //changing a certain class style
    //     function changeClassStyle(matchClass, content) {
    //         var elems = document.getElementsByTagName('*'), i;
    //         for (i in elems) {
    //             if((' ' + elems[i].className + ' ').indexOf(' ' + matchClass + ' ')
    //                     > -1) {
    //                 elems[i].setAttribute("style", content);
    //             }
    //         }
    //     }


        //creating the fade animation effect every 50 milliseconds
        function fadeTimeout()  {
            var mydiv = document.getElementById('thecanvas');
            var curr_opacity = window.getComputedStyle(mydiv,null).getPropertyValue("opacity");
            if(bCanvasClean == 1)
				return;
			else if (bMouseIsDown != 0)  {
                mydiv.style.opacity = 1;
                return;
            }
            else if(curr_opacity < 0.002){
                clearCanvas();
                mydiv.style.opacity = 1;
                return;
            }
            else{
                    curr_opacity = curr_opacity - 0.001;
            }
            mydiv.style.opacity = curr_opacity;
            setTimeout(fadeTimeout, 50);
        }

        //calling fadeAnimation every 2 seconds
        function penguinFade(){
            timeInterval = setInterval(fadeAnimation, 2000);
        }

        //checking if mouse is down in order to call fadeTimeout
        function fadeAnimation(){
            if ((bMouseIsDown == 0) && (bCanvasClean == 0))  {
                setTimeout(fadeTimeout,50);
            }
        }

        function saveCanvas(pCanvas, strType) {
            var bRes = false;
            if (strType == "PNG"){
                bRes = Canvas2Image.saveAsPNG(oCanvas);//, 1, 1000, 1000);
            }
            if (strType == "BMP")
                bRes = Canvas2Image.saveAsBMP(oCanvas);
            if (strType == "JPEG")
                bRes = Canvas2Image.saveAsJPEG(oCanvas);

            if (!bRes) {
                alert("Sorry, this browser is not capable of saving " + strType + " files!");
                return false;
            }
        }
    
    var Canvas2Image = (function() {

        // check if we have canvas support
        var bHasCanvas = false;
        var oCanvas = document.getElementById("thecanvas");
        if (oCanvas.getContext("2d")) {
            bHasCanvas = true;
        }

        // no canvas, bail out.
        if (!bHasCanvas) {
            return {
                saveAsBMP : function(){},
                saveAsPNG : function(){},
                saveAsJPEG : function(){}
            }
        }

        var bHasImageData = !!(oCanvas.getContext("2d").getImageData);
        var bHasDataURL = !!(oCanvas.toDataURL);
        var bHasBase64 = !!(window.btoa);

        var strDownloadMime = "image/octet-stream";

        var scaleCanvas = function(oCanvas, iWidth, iHeight) {
            if (iWidth && iHeight) {
                var oSaveCanvas = document.createElement("canvas");
                oSaveCanvas.width = iWidth;
                oSaveCanvas.height = iHeight;
                oSaveCanvas.style.width = iWidth+"px";
                oSaveCanvas.style.height = iHeight+"px";

                var oSaveCtx = oSaveCanvas.getContext("2d");

                oSaveCtx.drawImage(oCanvas, 0, 0, oCanvas.width, oCanvas.height, 0, 0, iWidth, iHeight);
                return oSaveCanvas;
            }
            return oCanvas;
        }

        return {

            saveAsPNG : function(oCanvas, bReturnImg, iWidth, iHeight) {
                if (!bHasDataURL) {
                    console.log("bHasDataURL has no data");
                    return false;
                }
                var oScaledCanvas = scaleCanvas(oCanvas, iWidth, iHeight);
                var strData = oScaledCanvas.toDataURL("image/png");
                if (bReturnImg) {
                    //console.log("THERE WAS A BRETURNIMG AFTER ALL\n");
                    //console.log(strData);
                    return makeImageObject(strData);
                } else {
                    //console.log("THERE WAS NO BRETURNIMG");
                    var save_button = document.getElementById("downloadbut");
                    save_button.href = strData;
                //  saveFile(strData.replace("image/png", strDownloadMime));
                }
                return true;
            }//,
        };

    })();

    var menu = $("#menu");
    var main = $("#main");
    var top = $("#top");
    var home = $("#home");
    var about = $("#about");
    var images = $("#images");
    var drawingCanvas = $("#drawingCanvas");
    var favorites = $("#favorites");
    var carousel = $("#carousel");
    var topsearch = $("#topsearch");
    var carousel_obj = new Carousel("#carousel");
    var is_favorites_active = false;
    var api_flickr = "http://api.flickr.com/services/feeds/photos_public.gne?jsoncallback=?";
    var imageGalleryInit = 0;

    // remove 300ms click delay in mobile browsers
    FastClick.attach(document.body);

    // associate all click handlers
    set_click_handlers();

    // clone menu hierarchy for large screen
    $("#top").append(menu.find("ul").clone());

    // url hash has changed
    $(window).on('hashchange', function () {
        route_url();
    });

    // load images from flickr
    //fetch_images();

    // route to main page
    route_url();

    // resize handler
    $(window).on('resize orientationchange', function(){
        resize_images();
    });

// based on url hash, adjust layout
function route_url() {
    var hash = window.location.hash;
    var fade_carousel = true;
    is_favorites_active = false;

    // select active menu item
    $("ul.menu").find("a").removeClass("active");
    var active = $("ul.menu").find("[href='"+(hash?hash:"#")+"']");
    active.addClass("active");
    top.find("h1 a").text(active.eq(0).text());

    if (!hash) {
        // home
        home.show();
        drawingCanvas.hide();
        images.hide();
        favorites.hide();
        about.hide();
        top.hide();
        $("#topsearch, #main").removeClass("opensearch");
    } else if (hash == "#gallery"){
        clearCanvas();
        $(".ico").attr("href", "#gallery");
        home.hide();
        drawingCanvas.hide();
        images.show();
        about.hide();
        favorites.hide();
        top.show();
        $("#localbut").show();
        $("#randombut").show();
        $("#downloadbut").hide();
        resize_images();
    } else if (hash == "#drawingCanvas"){
        $(".ico").attr("href", "#drawingCanvas");
        drawingCanvas.show();
        home.hide();
        images.hide();
        about.hide();
        top.show();
        favorites.hide();
        $("#localbut").hide();
        $("#randombut").hide();
        $("#downloadbut").show();
        //$("#topsearch, #main").removeClass("opensearch");
        penguinFade();
    } else if (hash == "#about") {
        // about
        images.hide();
        about.show();
        top.show();
        favorites.hide();
        $("#topsearch, #main").removeClass("opensearch");
    } else if (hash == "#favorites") {
        // favorites
        is_favorites_active = true;
        images.hide();
        about.hide();
        favorites.show();
        $("#topsearch, #main").removeClass("opensearch");
        fill_favorites();
        resize_images();
    } else if (hash.slice(0,5) == "#view") {
        // view image at index
        var i = parseInt(hash.slice(6));
        var div = images.find(">div").eq(i);
        show_carousel(div);
        about.hide();
        favorites.hide();
        fade_carousel = false;
    }

    if (fade_carousel && carousel.hasClass("anim2"))
        hide_carousel();
}


// resize images (width changed by CSS, height by this js)
function resize_images() {
    var cont = is_favorites_active ? favorites : images;
    var w = cont.find("div.my").eq(0).width();
    cont.find("div.my").height(w);
}

// sets all click handlers
function set_click_handlers() {
    $("#show_gallery").click(function(){
        //clearCanvas(); //clearing the canvas

        $("ul.menu").find("a").removeClass("active");
        var active = $("ul.menu").find("[href='#gallery']");
        active.addClass("active");
        top.find("h1 a").text(active.eq(0).text());
        $(".ico").attr("href", "#gallery");
        home.hide();
        images.show();
        drawingCanvas.hide();
        top.show();
        $("#localbut").show();
        $("#randombut").show();
        $("#downloadbut").hide();
        resize_images();
    });

    $("#show_canvas").click(function(){
        //clearCanvas(); //clearing the canvas

        $("ul.menu").find("a").removeClass("active");
        var active = $("ul.menu").find("[href='#drawingCanvas']");
        active.addClass("active");
        top.find("h1 a").text(active.eq(0).text());
        $(".ico").attr("href", "#drawingCanvas");
        home.hide();
        images.hide();
        drawingCanvas.show();
        top.show();
        $("#localbut").hide();
        $("#randombut").hide();
        $("#downloadbut").show();
        penguinFade();
    });

    $("#size10").click(function(){
		lineWidth = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * 10;
		oCtx.lineWidth = lineWidth;
        selectSize("size10");
    });
    $("#size20").click(function(){
        lineWidth = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * 20;
		oCtx.lineWidth = lineWidth;
        selectSize("size20");
    });
    $("#size30").click(function(){
        lineWidth = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * 30;
		oCtx.lineWidth = lineWidth;
        selectSize("size30");
    });
    $("#size40").click(function(){
        lineWidth = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * 40;
		oCtx.lineWidth = lineWidth;
        selectSize("size40");
    });
    $("#size50").click(function(){
		lineWidth = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * 50;	
		oCtx.lineWidth = lineWidth;
		console.log(lineWidth);
        selectSize("size50");
    });
    $("#normal_paintbrush").click(function(){
        paintbrush_type = 0;
		shadowBlur = 0;
        selectShadow("normal_paintbrush");
    });
    $("#wc_paintbrush").click(function(){
        paintbrush_type = 1;
		shadowBlur = Math.min(oCtx.canvas.width, oCtx.canvas.height) / 500 * waterColorBlur;
        selectShadow("wc_paintbrush");
    });

    // small screen: open/hide sliding menu
    $("#menubut, #menu a").click(function(){

        $("#main,#menu,#top,#topsearch").toggleClass("openmenu").removeClass("opensearch");

        if ($(this).attr("id") == "menubut")
            return false;
    });

    // reload all
    $("#reloadbut").click(function(){
        reload();
        // fall through to change tab
    });

    $("#localbut").click(function(){
        images.empty();
        local_search = 1;
        locateLocalPenguins();
    });

    $("#randombut").click(function(){
        images.empty();
        local_search = 0;
        random();
        // fall through to change tab
    });

    $("#downloadbut").click(function(){
        download();
        // fall through to change tab
    });

    // open up search
    $("#searchbut").click(function(){
       // console.log("WHY");
        $("#topsearch input").val("");
       if ($("#topsearch").toggleClass("opensearch").hasClass("opensearch"))
            setTimeout(function(){
                $("#topsearch input").focus();
            }, 500);
        main.toggleClass("opensearch");
        // fall through to change tab
    });


    // click on image
    $("#images, #favorites").on("click", "div", function(t) {
        var div = $(t.target);

        if (!div.is("div.my"))
            div = div.parents("div");
        if (!div.hasClass("my"))
            return;

        build_carousel(is_favorites_active ? favorites : images);

        if (div.hasClass("more"))
            load_more(div);
        else
            window.location.hash = "#view?" + div.index();
    });

    // click on image area closes menu
    main.on("click", function() {
        if (main.hasClass("openmenu")) {
            $("#menubut").click();
            return false;
        }
    });
	
	document.getElementById("clear_button").onclick = function(){
		clearCanvas();
	}


    // enter in search activates search
    $("#searchinput").keyup(function(e) {
        if (e.keyCode == 13) {
            reload();
            //add in another function to actually SEARCH for penguins
            return false;
        }
    });

    // escape always goes to home
    $(document.body).keyup(function(e) {
        if (e.keyCode == 27) {
            window.location.hash = "#gallery";
        }
    });

    /* won't work in all browsers since hammer.js is active;
       see handle_tap()

    // click left/right
    $(".fa-arrow-circle-o-left").click(function(){
        carousel.prev();
        return false;
    });
    $(".fa-arrow-circle-o-right").click(function(){
        carousel.next();
        return false;
    });
    // add/remove a favorite
    carousel.on("click", "i.fa-star", function() {
        toggle_favorite($(this));
    });
    */


}

// toggle favorite-status while in carousel
function toggle_favorite(t) {
    // t.toggleClass("favorited");

    // var url = t.closest("li").find("img").attr("src");
    // var title = t.closest("li").find("div").html();

    // if (t.hasClass("favorited"))
    //     add_favorite(url, title);
    // else
    //     remove_favorite(url);
}

function toggle_flag(t){
    t.toggleClass("flagged");

    if(t.hasClass("flagged")){
        //post flag information to backend
    }
}

// show carousel, position is at div
function show_carousel(div) {
    carousel_obj.init();
    carousel.hide().removeClass("anim1 anim2");

    // initial image index
    var i = div.index();
    carousel_obj.showPane(i, false);

    // show zoom anim
    carousel.addClass("anim1");
    carousel.show();
    carousel.width(); // flush
    carousel.addClass("anim2");

    $(window).scrollTop(0);
}

function hide_carousel() {
    carousel.removeClass("anim2");

    carousel.one("webkitTransitionEnd mozTransitionEnd transitionend", function(){
        // hide at end of transition
        carousel.hide();
    });
}

// discard and reload all images
function reload() {
    images.find("div").addClass("fadeaway");

    if(local_search){
        setTimeout(function() {
            images.empty();
            locateLocalPenguins();
        }, 1000);
    }else{
        setTimeout(function() {
            images.empty();
            fetch_images();
        }, 1000);
    }
}

function locateLocalPenguins(){
    //search for local penguins
    var latitude = 0;
    var longitude = 0;
    var data = {
      "code": 0,
      "message": [
          {"id": 1,
           "image": "images/penguin_thumbnails/button_color_white.png",
           "views": 1,
           "time": 1396579549
           },
          {"id": 2,
           "image": "images/penguin_thumbnails/button_color_yellow.png",
           "views": 1,
           "time": 1396579549
           },
          {"id": 3,
           "image": "images/penguin_thumbnails/ice_button.jpg",
           "views": 1,
           "time": 1396579549
           },
          {"id": 4,
           "image": "images/penguin_thumbnails/ice_frame2.jpg",
           "views": 1,
           "time": 1396579549
           },
          {"id": 5,
           "image": "images/penguin_thumbnails/ice_frame1.jpg",
           "views": 1,
           "time": 1396579549
           }
        ]
    }

    if (!navigator.geolocation){
      alert("<p>Geolocation is not supported by your browser</p>");
      return;
    }

    function success(position) {
      latitude  = position.coords.latitude;
      longitude = position.coords.longitude;
    };

    function error() {
      alert("Unable to retrieve your location");
    };

    navigator.geolocation.getCurrentPosition(success, error);

    // var galleryURL = BASE_URL + "penguins" + "?count=10&lat=latitude&long=longitude";
    // $.getJSON( galleryURL, function( data ) {
          if(data.code == 0){
                $.each( data.message, function( i, image ) {
                  $("<div>").attr("class", "my show").attr("id", "pengallery"+i).appendTo($("#images"));
                  $( "<img>" ).attr( "src", data.message[i].image ).appendTo("#pengallery"+i);
                  $("<div>").text("penguin "+i).appendTo("#pengallery"+i);
                });
              // });
              images.append($("#morediv").clone().removeAttr("id"));
              resize_images();
              //imageGalleryInit = 1;
          } else{ alert("Sorry, there was an internal error. Please try again later.");}
}

function random(){
    //fetch penguins randomly
    fetch_images();
}

function download(){
    //post penguin to backend
    //$.post()
   // saveCanvas(oCanvas, "PNG");
}

// load more images
function load_more(div) {
    div.find("i").addClass("fa-refresh");

    div.addClass("start");

    // can't use this since other can fire it
//    div.one("webkitTransitionEnd mozTransitionEnd transitionend", function(){
    if(local_search == 1){
        setTimeout(function() {
            images.find(".more").remove();
            locateLocalPenguins();
        }, 700);
    }
    else{
        setTimeout(function() {
            images.find(".more").remove();
            fetch_images();
        }, 700);
    }
}

// fetch images from Flickr
function fetch_images() {
    var data = {
      "code": 0,
      "message": [
          {"id": 1,
           "image": "images/penguin_thumbnails/button_color_black.png",
           "views": 1,
           "time": 1396579549
           },
          {"id": 2,
           "image": "images/penguin_thumbnails/button_color_blue.png",
           "views": 1,
           "time": 1396579549
           },
          {"id": 3,
           "image": "images/penguin_thumbnails/button_color_orange.png",
           "views": 1,
           "time": 1396579549
           },
          {"id": 4,
           "image": "images/penguin_thumbnails/button_color_red.png",
           "views": 1,
           "time": 1396579549
           },
          {"id": 5,
           "image": "images/penguin_thumbnails/button_color_purple.png",
           "views": 1,
           "time": 1396579549
           }
        ]
    }
   // if(imageGalleryInit == 0){

      // var galleryURL = BASE_URL + "penguins" + "?count=10";
      // $.getJSON( galleryURL, function( data ) {
            if(data.code == 0){
                  $.each( data.message, function( i, image ) {
                    $("<div>").attr("class", "my show").attr("id", "pengallery"+i).appendTo($("#images"));
                    $( "<img>" ).attr( "src", data.message[i].image ).appendTo("#pengallery"+i);
                    $("<div>").text("penguin "+i).appendTo("#pengallery"+i);
                  });
                // });
                images.append($("#morediv").clone().removeAttr("id"));
                resize_images();
                //imageGalleryInit = 1;
            } else{ alert("Sorry, there was an internal error. Please try again later.");}
  //  }
    // $.getJSON(api_flickr, {
    //     tags: $("#searchinput").val(),
    //     tagmode: "any",
    //     format: "json"
    // })
    // .done(function(data) {
    //     $.each(data.items, function(i, item) {
    //         var url = item.media.m;
    //         var title = item.title;
    //         var elem = $("<div class='my show'><img src='"+url+"'/><div>"+
    //                      title+"</div>");
    //         images.append(elem);
    //     });
    //     // add more-div and resize
    //     images.append($("#morediv").clone().removeAttr("id"));
    //     resize_images();
    // });

}

// build carousel <ul> from images in home/favorites
function build_carousel(container) {

    var ul = carousel.find("ul");
    ul.empty();

    container.find("div.my img").each(function(i, elem) {
        var t = $(elem).clone();
        ul.append(t);
        ul.find("img:last").wrap("<li/>");
        var li = ul.find("li:last");
        li.append("<div>"+$(elem).parent().find("div").html()+"</div>");

        // favorited?
        var f = "";
        var url = t.attr("src");
        if (is_favorite(url))
            f = "favorited";

        /* XXX fix portrait pics...
        if (t.height() > t.width())
             t.addClass("portrait");
        */

        li.append("<i class='fa fa-3x fa-times "+f+"'></i>");
        li.append("<i class='fa fa-3x fa-flag'></i>");
        // li.append("<i class='fa fa-3x fa-star "+f+"'></i>");
    });
}


// fill favorites div with data in localstorage
function fill_favorites() {
    var data = getdata();

    favorites.empty();

    if (!data.length) {
        favorites.append($("#favoempty").clone());
        return;
    }

    for (var i = 0; i < data.length; i++) {
        var obj = data[i];

        var elem = $("<div class='my show favorite'><img src='"+obj.url+"'/><div>"+
                     obj.title+"</div>");
        favorites.append(elem);
    }
}

function add_favorite(url, title) {
    var data = getdata();
    data.push({"title":title, "url":url});
    localStorage.setItem("imgdata", JSON.stringify(data));

    fill_favorites();
}

function remove_favorite(url) {
    var data = getdata();
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        if (url == obj.url) {
            data.splice(i, 1);
            break;
        }
    }

    localStorage.setItem("imgdata", JSON.stringify(data));

    fill_favorites();
}

function is_favorite(url) {
    var data = getdata();
    for (var i = 0; i < data.length; i++) {
        var obj = data[i];
        if (url == obj.url)
            return true;
    }
}
// return array containing favorite objects
function getdata() {
    var data = localStorage.getItem("imgdata");
    if (!data)
        return [];
    return JSON.parse(data);
}


/*--------------------------------------------------------------------------*/
// carousel component from hammer.js demo


// handle taps of <i> in the carousel
function handle_tap(el) {
    if (!el.is("i"))
       // window.history.back();
        window.location.hash = "#gallery";
    else if (el.is(".fa-times"))
        window.location.hash = "#gallery";
       // window.history.back();
    else if(el.is(".fa-flag"))
        toggle_flag(el);
    //else if (el.is(".fa-star"))
        //toggle_favorite(el);
    else if (el.is(".fa-arrow-circle-o-left"))
        carousel_obj.prev();
    else if (el.is(".fa-arrow-circle-o-right"))
        carousel_obj.next();
}

/**
* super simple carousel
* animation between panes happens with css transitions
*/
function Carousel(element)
{
    var self = this;
    element = $(element);

    var container = $(">ul", element);
    var panes = $(">ul>li", element);

    var pane_width = 0;
    var pane_count = panes.length;

    var current_pane = 0;


    /**
     * initial
     */
    this.init = function() {
        panes = $(">ul>li", element);
        pane_count = panes.length;

        setPaneDimensions();

        $(window).on("load resize orientationchange", function() {
            setPaneDimensions();
            //updateOffset();
        });
    };


    /**
     * set the pane dimensions and scale the container
     */
    function setPaneDimensions() {
        pane_width = element.width();
        panes.each(function() {
            $(this).width(pane_width);
        });
        container.width(pane_width*pane_count);
    }

    /**
     * show pane by index
     */
    this.showPane = function(index, animate) {
        // between the bounds
        index = Math.max(0, Math.min(index, pane_count-1));
        current_pane = index;

        var offset = -((100/pane_count)*current_pane);
        setContainerOffset(offset, animate);
    };


    function setContainerOffset(percent, animate) {
        container.removeClass("animate");

        if(animate) {
            container.addClass("animate");
        }

        if(Modernizr.csstransforms3d) {
            container.css("transform", "translate3d("+ percent +"%,0,0) scale3d(1,1,1)");
        }
        else if(Modernizr.csstransforms) {
            container.css("transform", "translate("+ percent +"%,0)");
        }
        else {
            var px = ((pane_width*pane_count) / 100) * percent;
            container.css("left", px+"px");
        }
    }

    this.next = function() { return this.showPane(current_pane+1, true); };
    this.prev = function() { return this.showPane(current_pane-1, true); };

    function handleHammer(ev) {
        // disable browser scrolling
        if (ev.gesture)
            ev.gesture.preventDefault();

        switch(ev.type) {
            case 'dragright':
            case 'dragleft':
                // stick to the finger
                var pane_offset = -(100/pane_count)*current_pane;
                var drag_offset = ((100/pane_width)*ev.gesture.deltaX) / pane_count;

                // slow down at the first and last pane
                if((current_pane == 0 && ev.gesture.direction == "right") ||
                    (current_pane == pane_count-1 && ev.gesture.direction == "left")) {
                    drag_offset *= .4;
                }

                setContainerOffset(drag_offset + pane_offset);
                break;

            case 'swipeleft':
                self.next();
                ev.gesture.stopDetect();
                break;

            case 'swiperight':
                self.prev();
                ev.gesture.stopDetect();
                break;

            case 'tap':
                handle_tap($(ev.target));
                break;

            case 'release':
                // more then 50% moved, navigate
                if(Math.abs(ev.gesture.deltaX) > pane_width/2) {
                    if(ev.gesture.direction == 'right') {
                        self.prev();
                    } else {
                        self.next();
                    }
                }
                else {
                    self.showPane(current_pane, true);
                }
                break;
            default:
                break;
        }
    }

    var hammertime = new Hammer(element[0], { drag_lock_to_axis: true,
        drag_block_vertical: true});
    hammertime.on("release dragleft dragright swipeleft swiperight tap",
                  handleHammer);
}



});

